#+TITLE: Interface Utilisateur Graphique : TP
#+AUTHOR: Bernard Hugueney
#+DATE: <2021-05-05 Wed 09:00>
#+LANGUAGE:  fr

#+BEGIN_SRC emacs-lisp :exports none :results silent
(setq ob-ipython-show-mime-types nil)
#+END_SRC

* Amélioration

L'affichage étant un peu long, on voudra éviter de le déclencher à chaque modification d'un des /widgets/ de l'interface graphique. On peut ajouter un bouton qui servira à déclencher l'affichage une fois qu'on aura fini de modifier tous les paramètres.

* Évolution

On pourra vouloir afficher les données non pas sur une année, mais sur un seul jour. On peut mettre des onglets pour sélectionner le type de visualisation que l'on veut.

#+BEGIN_SRC ipython
import ipywidgets as widgets
from IPython.display import display
#+END_SRC


#+BEGIN_SRC ipython
stations="""07005	ABBEVILLE
07015	LILLE-LESQUIN
07020	PTE DE LA HAGUE
07027	CAEN-CARPIQUET
07037	ROUEN-BOOS
07072	REIMS-PRUNAY
07110	BREST-GUIPAVAS
07117	PLOUMANAC'H
07130	RENNES-ST JACQUES
07139	ALENCON
07149	ORLY
07168	TROYES-BARBEREY
07181	NANCY-OCHEY
07190	STRASBOURG-ENTZHEIM
07207	BELLE ILE-LE TALUT
07222	NANTES-BOUGUENAIS
07240	TOURS
07255	BOURGES
07280	DIJON-LONGVIC
07299	BALE-MULHOUSE
07314	PTE DE CHASSIRON
07335	POITIERS-BIARD
07434	LIMOGES-BELLEGARDE
07460	CLERMONT-FD
07471	LE PUY-LOUDES
07481	LYON-ST EXUPERY
07510	BORDEAUX-MERIGNAC
07535	GOURDON
07558	MILLAU
07577	MONTELIMAR
07591	EMBRUN
07607	MONT-DE-MARSAN
07621	TARBES-OSSUN
07627	ST GIRONS
07630	TOULOUSE-BLAGNAC
07643	MONTPELLIER
07650	MARIGNANE
07661	CAP CEPET
07690	NICE
07747	PERPIGNAN
07761	AJACCIO
07790	BASTIA
61968	GLORIEUSES
61970	JUAN DE NOVA
61972	EUROPA
61976	TROMELIN
61980	GILLOT-AEROPORT
61996	NOUVELLE AMSTERDAM
61997	CROZET
61998	KERGUELEN
67005	PAMANDZI
71805	ST-PIERRE
78890	LA DESIRADE METEO
78894	ST-BARTHELEMY METEO
78897	LE RAIZET AERO
78922	TRINITE-CARAVEL
78925	LAMENTIN-AERO
81401	SAINT LAURENT
81405	CAYENNE-MATOURY
81408	SAINT GEORGES
81415	MARIPASOULA
89642	DUMONT D'URVILLE"""

name_to_code={}
for t in stations.split('\n'):
    code, name= t.split('\t')
    name_to_code[name]=int(code)
#+END_SRC

#+BEGIN_SRC ipython
gui_station = widgets.Dropdown(
    options=name_to_code.keys(),
    value='ORLY',
    description='Station:',
)
#+END_SRC


#+BEGIN_SRC ipython
desc_to_columns={"Vitesse du vent moyen 10 mn (m/s)":'ff',
        "Température (°C)":'t',
        "Humidité (%)":'u',
        "Visibilité horizontale (m)" : 'vv',
        "Nebulosité totale (%)" : 'n',
        "Pression station (Pa)" : 'pres',
        "Hauteur totale de la couche de neige, glace, autre (m)": 'ht_neige',
        "Précipitation dans la dernière heure (mm)" : 'rr1'}
#+END_SRC

#+BEGIN_SRC ipython
gui_mesure = widgets.Dropdown(
    options=desc_to_columns.keys(),
    value="Température (°C)",
    description='Mesure:',
)
#+END_SRC

#+BEGIN_SRC ipython
import pandas as pd
import seaborn as sns; sns.set(rc={'figure.figsize':(20,10)})
#+END_SRC

#+BEGIN_SRC ipython
df=pd.read_csv("./Data/Meteo/meteo_france.csv.zip",parse_dates=['date'])
df['t']=df['t']-273.15 # K → C
#+END_SRC

#+BEGIN_SRC ipython
import datetime
#+END_SRC

#+BEGIN_SRC ipython
gui_year = widgets.Dropdown(
    options=df['date'].dt.year.astype('str').astype('object').unique(),
    value='2019',
    description='Année:',
)
#+END_SRC



#+BEGIN_SRC ipython
import matplotlib.pyplot as plt
import matplotlib.dates as md
from IPython.display import clear_output
#+END_SRC

#+BEGIN_SRC ipython
out_year= widgets.Output()
def do_plot_year(unused):
    ax=sns.lineplot(data=df.loc[df['numer_sta']==name_to_code[gui_station.value],].set_index('date').loc[gui_year.value],
                x='date', y=desc_to_columns[gui_mesure.value])
    ax.xaxis.set_major_locator(md.WeekdayLocator(byweekday = 1))
    ax.xaxis.set_minor_locator(md.DayLocator())
    ax.xaxis.set_major_formatter(md.DateFormatter('%Y-%m-%d'))
    plt.setp(ax.xaxis.get_majorticklabels(), rotation = 90)
    ax.xaxis.set_minor_locator(md.DayLocator(interval = 1))
    ax.set_xlabel('Date')
    ax.set_ylabel(gui_mesure.value)
    ax.set_title("Relevés à "+gui_station.value+" en "+gui_year.value)
    with out_year :
        clear_output(wait=True)
        plt.show()
#+END_SRC

#+BEGIN_SRC ipython
#gui_station.observe(do_plot_year)
#gui_mesure.observe(do_plot_year)
#gui_year.observe(do_plot_year)
#display(gui_station, gui_mesure, gui_year);
#+END_SRC

#+BEGIN_SRC ipython
do_plot_year(None)
#display(out_year);
#+END_SRC


#+BEGIN_SRC ipython
import ipywidgets as widgets
from IPython.display import display
#+END_SRC

#+BEGIN_SRC ipython
gui_day = widgets.DatePicker(
    description='date :',
    value= datetime.date.fromisoformat('2019-01-01'),
    disabled=False
)
#+END_SRC

#+BEGIN_SRC ipython
out_day= widgets.Output()
def do_plot_day(unused):
    day=str(gui_day.value)
    ax=sns.lineplot(data=df.loc[df['numer_sta']==name_to_code[gui_station.value],].set_index('date').loc[day],
                x='date', y=desc_to_columns[gui_mesure.value])
    ax.xaxis.set_major_formatter(md.DateFormatter('%H:%M'))
#    plt.setp(ax.xaxis.get_majorticklabels(), rotation = 90)
    ax.set_xlabel('Date')
    ax.set_ylabel(gui_mesure.value)
    ax.set_title("Relevés à "+gui_station.value+" le "+day)
    with out_day :
        clear_output(wait=True)
        plt.show()
#+END_SRC


#+BEGIN_SRC ipython
#gui_station.observe(do_plot_day)
#gui_mesure.observe(do_plot_day)
#gui_day.observe(do_plot_day)
#display(gui_station, gui_mesure, gui_day);
#+END_SRC

#+BEGIN_SRC ipython
do_plot_day(None)
#display(out_day);
#+END_SRC


#+BEGIN_SRC ipython
button_year = widgets.Button(description="Afficher !")
button_year.on_click(do_plot_year)
tab1 = widgets.VBox(children=[gui_year, button_year, out_year])
button_day = widgets.Button(description="Afficher !")
button_day.on_click(do_plot_day)
tab2 = widgets.VBox(children=[gui_day, button_day, out_day])
tab = widgets.Tab(children=[tab1, tab2])
tab.set_title(0, 'Année')
tab.set_title(1, 'Jour')
display(gui_station, gui_mesure, tab);
#+END_SRC
