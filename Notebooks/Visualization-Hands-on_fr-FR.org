#+TITLE: Visualisation de données avec Seaborn : Travaux Pratiques
#+AUTHOR: Bernard Hugueney
#+DATE: <2020-12-07 Mon 16:00>
#+LANGUAGE:  fr



#+BEGIN_SRC emacs-lisp :exports none :results silent
(setq ob-ipython-show-mime-types nil)
#+END_SRC


On voudra visualiser les informations apportées par le jeu de données
sur les diamants, /dioamonds/, qui est mis à disposition comme jeu
d'exemples par la bibliothèque /Seaborn/. On peut le charger comme
suit :

#+BEGIN_SRC ipython
%matplotlib inline
import pandas as pd
import matplotlib.pyplot as plt
import seaborn as sns
diamonds = sns.load_dataset("diamonds")
#+END_SRC

*Remarque :* Si le chargement échoue lors d'une exécution en local à
cause de restrictions de sécurité réseau qui exigent l'utilisation
d'un proxy, on peut télécharger [[https://raw.githubusercontent.com/mwaskom/seaborn-data/master/diamonds.csv][le fichier]] à l'aide du navigateur et
utiliser ensuite directement ce fichier à la place.


* Informations générales

Commencer par afficher les noms des colonnes, puis les types des
colonnes et la taille en mémoire de la ~DataFrame~.


#+BEGIN_SRC ipython
#%load hints/exo_visu_1-1.py
# À faire !
#+END_SRC

#+BEGIN_SRC ipython :mkdirp yes :exports none :tangle ./hints/exo_visu_1-1.py
#%load solutions/exo_visu_1.py
help(diamonds.info)
#+END_SRC

#+BEGIN_SRC ipython :mkdirp yes :exports none :tangle ./solutions/exo_visu_1.py
diamonds.info()
#+END_SRC


* Valeurs manquantes ou aberrantes

** Localisation

Chercher s'il y a des valeurs manquantes ou aberrantes (par exemple des dimensions à 0).

#+BEGIN_SRC ipython
#%load hints/exo_visu_2-1.py
# À faire !
#+END_SRC

#+BEGIN_SRC ipython :mkdirp yes :exports none :tangle ./hints/exo_visu_2-1.py
#%load solutions/exo_visu_2.py
# test values == 0 and count the True results with the sum() method
#TODO
#+END_SRC

#+BEGIN_SRC ipython :mkdirp yes :exports none :tangle ./solutions/exo_visu_2.py
# one can check that diamonds.isnull().sum() finds 0 NA
(diamonds==0).sum()
#+END_SRC

** Corrections

Est-ce qu'il est préférable de remplacer les valeurs aberrantes ou de
supprimer les lignes ou colonnes contenant des valeurs aberrantes ?

Supprimer les lignes avec valeurs aberrantes.

#+BEGIN_SRC ipython
#%load hints/exo_visu_3-1.py
#TODO
#+END_SRC

#+BEGIN_SRC ipython :mkdirp yes :exports none :tangle ./hints/exo_visu_3-1.py
#%load solutions/exo_visu_3.py
# compare DataFrame to 0 and select rows where all values are !=0
# replace diamonds with the selection of only those rows
#TODO
#+END_SRC

#+BEGIN_SRC ipython :mkdirp yes :exports none :tangle ./solutions/exo_visu_3.py
diamonds=diamonds[(diamonds != 0).all(axis=1)]
#+END_SRC

Vérifier qu'il n'y a plus de valeurs aberrantes :


#+BEGIN_SRC ipython
#%load hints/exo_visu_4-1.py
#TODO
#+END_SRC

#+BEGIN_SRC ipython :mkdirp yes :exports none :tangle ./hints/exo_visu_4-1.py
#%load solutions/exo_visu_4.py
# same as above !
#TODO
#+END_SRC

#+BEGIN_SRC ipython :mkdirp yes :exports none :tangle ./solutions/exo_visu_4.py
(diamonds==0).sum()
#+END_SRC

* Tailles des diamants en carats

Afficher les moyenne, déviations standard, et quartiles de la taille
en carats des diamants :

#+BEGIN_SRC ipython
#%load hints/exo_visu_5-1.py
#TODO
#+END_SRC

#+BEGIN_SRC ipython :mkdirp yes :exports none :tangle ./hints/exo_visu_5-1.py
#%load solutions/exo_visu_5.py
# https://pandas.pydata.org/pandas-docs/stable/reference/api/pandas.Series.describe.html
#TODO
#+END_SRC

#+BEGIN_SRC ipython :mkdirp yes :exports none :tangle ./solutions/exo_visu_5.py
diamonds['carat'].describe()
#+END_SRC


Afficher l'histogramme de la taille des diamants en carats :

#+BEGIN_SRC ipython
#%load hints/exo_visu_6-1.py
#TODO
#+END_SRC

#+BEGIN_SRC ipython :mkdirp yes :exports none :tangle ./hints/exo_visu_6-1.py
#%load solutions/exo_visu_6.py
help(sns.distplot)
#TODO
#+END_SRC

#+BEGIN_SRC ipython :mkdirp yes :exports none :tangle ./solutions/exo_visu_6.py
sns.distplot(diamonds['carat'], kde=False)
#+END_SRC


Utiliser 100 subdivisions (/bins/) de la taille en carats pour
afficher l'histogramme.

#+BEGIN_SRC ipython
#%load hints/exo_visu_7-1.py
#TODO
#+END_SRC

#+BEGIN_SRC ipython :mkdirp yes :exports none :tangle ./hints/exo_visu_7-1.py
#%load solutions/exo_visu_7.py
help(sns.distplot)
#TODO
#+END_SRC

#+BEGIN_SRC ipython :mkdirp yes :exports none :tangle ./solutions/exo_visu_7.py
sns.distplot(diamonds['carat'], bins=100, kde=False)
#+END_SRC

Afficher la distribution de la /table/ des diamants :

#+BEGIN_SRC ipython
#%load hints/exo_visu_8-1.py
#TODO
#+END_SRC

#+BEGIN_SRC ipython :mkdirp yes :exports none :tangle ./hints/exo_visu_8-1.py
#%load solutions/exo_visu_8.py
help(sns.distplot)
#TODO
#+END_SRC

#+BEGIN_SRC ipython :mkdirp yes :exports none :tangle ./solutions/exo_visu_8.py
sns.distplot(diamonds["table"])
#+END_SRC

Afficher la distribution du prix des diamants :

#+BEGIN_SRC ipython
#%load hints/exo_visu_8-1.py
#TODO
#+END_SRC

#+BEGIN_SRC ipython :mkdirp yes :exports none :tangle ./hints/exo_visu_8-1.py
#%load solutions/exo_visu_8.py
help(sns.distplot)
#TODO
#+END_SRC

#+BEGIN_SRC ipython :mkdirp yes :exports none :tangle ./solutions/exo_visu_8.py
sns.distplot(diamonds["price"])
#+END_SRC

Afficher les distributions des dimension ~x~, ~y~ et ~z~.

#+BEGIN_SRC ipython
#%load hints/exo_visu_9-1.py
#TODO
#+END_SRC

#+BEGIN_SRC ipython :mkdirp yes :exports none :tangle ./hints/exo_visu_9-1.py
#%load solutions/exo_visu_9.py
help(plt.subplots)
# use the ax argument of distplot
#TODO
#+END_SRC

#+BEGIN_SRC ipython :mkdirp yes :exports none :tangle ./solutions/exo_visu_9.py
fig, axs = plt.subplots(nrows=3)
fig.set_size_inches(16,11)
sns.distplot(diamonds["x"], ax=axs[0])
sns.distplot(diamonds["y"], ax=axs[1])
sns.distplot(diamonds["z"], ax=axs[2]);
#+END_SRC

Représenter graphiquement les nombre de diamants dans chaque catégorie
de qualité de taille (~cut~): 'Ideal','Premium','Very Good', 'Good',
'Fair' :

#+BEGIN_SRC ipython
#%load hints/exo_visu_10-1.py
#TODO
#+END_SRC

#+BEGIN_SRC ipython :mkdirp yes :exports none :tangle ./hints/exo_visu_10-1.py
#%load solutions/exo_visu_10.py
help(sns.catplot)
# use the order argument of catplot
#TODO
#+END_SRC

#+BEGIN_SRC ipython :mkdirp yes :exports none :tangle ./solutions/exo_visu_10.py
sns.catplot(x="cut", kind="count", data=diamonds, order=['Ideal','Premium','Very Good', 'Good', 'Fair'])
#+END_SRC

Représenter graphiquement le nombre de diamants dans chaque catégorie
de couleur :

#+BEGIN_SRC ipython
#%load hints/exo_visu_11-1.py
#TODO
#+END_SRC

#+BEGIN_SRC ipython :mkdirp yes :exports none :tangle ./hints/exo_visu_11-1.py
#%load solutions/exo_visu_11.py
help(sns.catplot)
# use the order argument of catplot
#TODO
#+END_SRC

#+BEGIN_SRC ipython :mkdirp yes :exports none :tangle ./solutions/exo_visu_11.py
sns.catplot(x="color", kind="count", data=diamonds, order=sorted(list("DEFGHIJ")))
#+END_SRC

Représenter graphiquement le nombre de diamants dans chaque catégorie
de clareté (/clarity/): 'F','IF', 'WS1','WS2','VS1','VS2', 'SI1','SI2',
'I1', 'I2', 'I3'

#+BEGIN_SRC ipython
#%load hints/exo_visu_12-1.py
#TODO
#+END_SRC

#+BEGIN_SRC ipython :mkdirp yes :exports none :tangle ./hints/exo_visu_12-1.py
#%load solutions/exo_visu_12.py
# use the order argument of catplot
#TODO
#+END_SRC

#+BEGIN_SRC ipython :mkdirp yes :exports none :tangle ./solutions/exo_visu_12.py
sns.catplot(x="clarity", kind="count", data=diamonds, order=['F','IF', 'VVS1', 'VVS2', 'VS1', 'VS2', 'SI1', 'SI2','I1', 'I2', 'I3'])
#+END_SRC


Pour chacune des catégorie de clareté, représenter graphiquement le
rapport entre la taille en carats et le prix :

#+BEGIN_SRC ipython
#%load hints/exo_visu_13-1.py
#TODO
#+END_SRC

#+BEGIN_SRC ipython :mkdirp yes :exports none :tangle ./hints/exo_visu_13-1.py
#%load solutions/exo_visu_13.py
help(sns.relplot)
#TODO
#+END_SRC

#+BEGIN_SRC ipython :mkdirp yes :exports none :tangle ./solutions/exo_visu_13.py
sns.relplot(x='carat', y='price', data=diamonds, hue='color',style='cut',height=16, col='clarity',col_wrap=3)
#+END_SRC

Représenter graphiquement la relation entre le prix et la dimension
~x~ :

#+BEGIN_SRC ipython
# À faire !
#+END_SRC

Représenter graphiquement la relation entre la taille en carats et la
dimension ~x~ :

#+BEGIN_SRC ipython
#%load hints/exo_visu_14-1.py
#TODO
#+END_SRC

#+BEGIN_SRC ipython :mkdirp yes :exports none :tangle ./hints/exo_visu_14-1.py
#%load solutions/exo_visu_14.py
help(sns.relplot)
#TODO
#+END_SRC

#+BEGIN_SRC ipython :mkdirp yes :exports none :tangle ./solutions/exo_visu_14.py
sns.relplot(y="carat", x='x', data=diamonds)
#+END_SRC

Représenter graphiquement les distributions de prix pour chaque
catégorie de taille :

#+BEGIN_SRC ipython
#%load hints/exo_visu_15-1.py
#TODO
#+END_SRC

#+BEGIN_SRC ipython :mkdirp yes :exports none :tangle ./hints/exo_visu_15-1.py
#%load solutions/exo_visu_15.py
help(sns.catplot)
#TODO
#+END_SRC

#+BEGIN_SRC ipython :mkdirp yes :exports none :tangle ./solutions/exo_visu_15.py
sns.catplot(x="cut", y="price",kind="violin", data=diamonds)
#+END_SRC

Représenter graphiquement les relations entre toutes les paires
d'attributs :

#+BEGIN_SRC ipython
#%load hints/exo_visu_16-1.py
#TODO
#+END_SRC

#+BEGIN_SRC ipython :mkdirp yes :exports none :tangle ./hints/exo_visu_16-1.py
#%load solutions/exo_visu_16.py
help(sns.pairplot)
#TODO
#+END_SRC

#+BEGIN_SRC ipython :mkdirp yes :exports none :tangle ./solutions/exo_visu_16.py
sns.pairplot(diamonds)
#+END_SRC

Représenter graphiquement les corrélations entre toutes les paires
d'attributs :

#+BEGIN_SRC ipython
#%load hints/exo_visu_17-1.py
#TODO
#+END_SRC

#+BEGIN_SRC ipython :mkdirp yes :exports none :tangle ./hints/exo_visu_17-1.py
#%load solutions/exo_visu_17.py
help(diamonds.corr)
help(sns.heatmap)
#TODO
#+END_SRC

#+BEGIN_SRC ipython :mkdirp yes :exports none :tangle ./solutions/exo_visu_17.py
plt.figure(figsize = (16,16))
sns.heatmap(diamonds.corr())
#+END_SRC
