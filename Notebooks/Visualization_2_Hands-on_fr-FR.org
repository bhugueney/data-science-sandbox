#+TITLE: Visualisation de données avec Seaborn (paramétrages) : Travaux Pratiques
#+AUTHOR: Bernard Hugueney
#+DATE: <2021-05-03 Mon 09:00>
#+LANGUAGE:  fr



#+BEGIN_SRC emacs-lisp :exports none :results silent
(setq ob-ipython-show-mime-types nil)
#+END_SRC

* Jeu de données

Charger le [[https://seaborn.pydata.org/generated/seaborn.load_dataset.html][jeu de données Seaborn]] 'tips'.

* Paramétrage général

  1. Afficher l'histogramme des tailles de groupes. Augmenter la
     taille des labels (en x et en y) pour avoir une taille 20.
  2. Afficher la grille (traits horizontaux et verticaux sur les
     positions des /ticks/) en gris foncé (='darkgrey'=).

* Palette de couleur

Choisir de façon interactive une nouvelle palette de couleur et
l'utiliser pour afficher l'histogramme des tailles de groupes en
distinguant suivant les jours de semaine (on 'empilera' les
histogrammes au lieu de les superposer).


* Titre et légendes

Toujours sur l'histogramme de la taille des groupes, ajouter le titre
"Histogrammes des taille des groupes" et mettez des légendes "Taille
du groupe" et "Nombre de groupes" sur les axes x et y.

* Texte

Toujours sur l'histogramme de la taille des groupes, ajouter le
nombre de groupes de 2 personnes sur le graphe.

* Annotation

Mettre une flèche vers le sommet de la colonne correspondant aux
groupes de 2 personnes pour indiquer, sous forme de texte, quel est le
nombre de groupes de deux personnes.

* Lignes verticales ou horizontales

1. Sur un /scatterplot/ du pourboire en fonction du montant total de
l'addition, indiquer par un trait horizontal la moyenne des pourboires
et par un trait horizontal la moyenne des montants d'addition.

2. Sur graphe des distributions de montants de pourboire des hommes et
   des femmes, indiquez par des traits verticaux les montants médians
   des pourboires des hommes et des pourboires des femmes.


* Remplissage de région entre deux lignes verticales ou horizontales

Sur un /scatterplot/ du pourboire en fonction du montant total de
l'addition, indiquer par un trait horizontal la médiane des pourboires
et par un trait horizontal la médiane des montants d'addition.
Indiquer aussi par une bande horizontale les pourboires des deuxième
et troisième quartiles (entre 25% et 75%) et par une bande verticale
les montants des deuxième et troisième quartiles.

* Sélection des intervalles de coordonnées représentés

  Sur un /scatterplot/ du pourboire en fonction du montant total de
l'addition, limiter l'affichage à un maximum de $20 pour le montant
total de l'addition.

* Visualisation de modèle linéaire

  1. Avec un petit graphe par jour, en distinguant les hommes et les
     femmes par la couleur et par les /markers/ (=♂= pour les hommes
     et =♀= pour les femmes), représenter un modèle linéaire du
     montant du pourboire en fonction du montant total de
     l'addition. On augmentera la taille et celle des traits
     représentant les modèles linéaires.
  2. Même chose en ayant cette fois un graphe pour les hommes et un
     pour les femmes et en distinguant les jours de semaine dans
     chaque graphe, par des couleurs et par des marqueurs (=J= pour
     jeudi, =V= pour vendredi, =S= pour samedi et =D= pour dimanche).


** Paramétrage a posteriori

 Pour le dernier graphe (avec les deux petits graphes, un pour les
 hommes et un pour les femmes), changer les titres et labels pour
 qu'ils soient en Français.

* Distributions jointes (jointplot)

  Visualiser la distribution jointe des pourboires (en y) et des
  montants d'addition (en x).  Superposer un /scatterplot/ et un
  /kdeplot/ au centre.  Indiquer par des traits les moyennes sur les
  distributions marginales.

  Indiquer aussi les écarts types autour de la moyenne avec des bandes
  de couleurs.
  
