#+TITLE: Interface Utilisateur Graphique
#+AUTHOR: Bernard Hugueney
#+DATE: <2021-05-05 Wed 09:00>
#+LANGUAGE:  fr

#+BEGIN_SRC emacs-lisp :exports none :results silent
(setq ob-ipython-show-mime-types nil)
#+END_SRC

* GUI
   :PROPERTIES:
   :metadata: (slideshow . ((slide_type . slide)))
   :END:
* interact
   :PROPERTIES:
   :metadata: (slideshow . ((slide_type . slide)))
   :END:

Permet de choisir à la souris les paramètres de l'appel d'une
fonction.
   
* ipywidgets
   :PROPERTIES:
   :metadata: (slideshow . ((slide_type . slide)))
   :END:

Permet de sélectionner / saisir des valeurs qui peuvent être utilisées
comme n'importe quelle variable. Un même widget peut être affiché
plusieurs fois / en plusieurs endroits.

* ipyvuetify
   :PROPERTIES:
   :metadata: (slideshow . ((slide_type . slide)))
   :END:

Des /widgets/ modernes :

[[https://user-images.githubusercontent.com/46192475/79730684-78954880-82f1-11ea-855b-43a2b619ca04.gif]]


* Appmode
   :PROPERTIES:
   :metadata: (slideshow . ((slide_type . slide)))
   :END:
Permet de ne laisser apparents que les widgets d'interface graphique.

   
* Voila
   :PROPERTIES:
   :metadata: (slideshow . ((slide_type . slide)))
   :END:

Permet de remplacer le serveur de Notebooks, /jupyter/, par un serveur
qui ne gère que les widgets, sans possibilité de voir / modifier le
code (\rightarrow sécurité).


[[https://mybinder.org/v2/gh/sunnysinghnitb/covid-19-world-dashboard/master?filepath=%2Fvoila%2Frender%2Fcovid_19_dashboard.ipynb][Visualisation interactive de l'impact du covid-19 dans le monde]]

[[https://github.com/sunnysinghnitb/covid-19-world-dashboard/blob/master/covid_19_dashboard.ipynb][Notebook Jupyter]]

* Exemples

** interact
  #+BEGIN_SRC ipython
from IPython.display import SVG
from graphviz import Source
from IPython.display import display                               
from ipywidgets import interactive
from sklearn.datasets import load_iris
from sklearn.tree import DecisionTreeClassifier, plot_tree, export_graphviz

iris= load_iris()
X = iris.data
y = iris.target
labels = iris.feature_names
def plot_tree(crit, split, depth, min_split, min_leaf=0.2):
    estimator = DecisionTreeClassifier(random_state = 0 
      , criterion = crit
      , splitter = split
      , max_depth = depth
      , min_samples_split=min_split
      , min_samples_leaf=min_leaf)
    estimator.fit(X, y)
    graph = Source(export_graphviz(estimator
      , out_file=None
      , feature_names=labels
      , class_names=['0', '1', '2']
      , filled = True))
   
    display(SVG(graph.pipe(format='svg')))
    return estimator
inter=interactive(plot_tree 
   , crit = ["gini", "entropy"]
   , split = ["best", "random"]
   , depth=range(1,10)
   , min_split=(0.1,1)
   , min_leaf=(0.1,0.5))
display(inter)
  #+END_SRC

** ipywidgets

   
#+BEGIN_SRC ipython
import ipywidgets as widgets
from IPython.display import display
w = widgets.IntSlider()
display(w)
2 # une autre valeur en fin de cellule, qui sera donc affichée automatiquement
#+END_SRC

#+BEGIN_SRC ipython
w
#+END_SRC

[[https://ipywidgets.readthedocs.io/en/latest/_images/assoc.svg]]

#+BEGIN_SRC ipython
w.value
#+END_SRC

#+BEGIN_SRC ipython
w.value = 55
#+END_SRC

#+BEGIN_SRC ipython
button = widgets.Button(description="Appuyez !")
output = widgets.Output()

display(button, output)

def on_button_clicked(b):
    with output:
        print("Vous avez cliqué sur le bouton !")

button.on_click(on_button_clicked)
#+END_SRC
