sns.set(font_scale=2)
from statsmodels.tsa.seasonal import seasonal_decompose
decomposition = seasonal_decompose(deaths['count'], model='additive',freq=int(365.25))
fig = decomposition.plot()
fig.set_size_inches(18,18);
