from sklearn.metrics import mean_squared_error

def forecast(ts, split_date):
    ts_train= ts[:split_date]['count'].to_frame()
    ts_test= ts.loc[ts.index > split_date]['count'].to_frame()
    ts_train.index=ts_train.index.tz_localize(None)
    model = Prophet()
    model.fit(ts_train.reset_index().rename(columns={'date_deces':'ds','count':'y'}))
    ts_test.index=ts_test.index.tz_localize(None)
    ts_test_fcst = model.predict(df=ts_test.reset_index().rename(columns={'date_deces':'ds'}))
    f, ax = plt.subplots(1)
    f.set_figheight(5)
    f.set_figwidth(15)
    ax.scatter(ts_test.index, ts_test['count'], color='r', s=5)
    fig = model.plot(ts_test_fcst, ax=ax)
    ax.set_xbound([datetime.datetime.strptime(split_date, '%Y-%m-%d')-datetime.timedelta(days=30),
                   ts_test.index.max()])
    plt.title("RMSE : %f" % mean_squared_error(ts_test['count'],ts_test_fcst['yhat'] ))
    return fig
