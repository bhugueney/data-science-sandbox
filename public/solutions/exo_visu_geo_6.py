ax= plt.axes(projection = tiles.crs)
ax.set_extent([2.22, 2.46, 48.81, 48.9], ccrs.PlateCarree())
ax.add_image(tiles, 12)
geo_vergers.plot(ax=ax, c='g', markersize=128, transform = ccrs.PlateCarree())
