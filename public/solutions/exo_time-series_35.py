sns.set(font_scale=5)
sns.catplot(y='t', x='month', kind='violin',col='year',col_wrap=4,data=local_temps, height=16)
plt.title("Temperature (°K)");
