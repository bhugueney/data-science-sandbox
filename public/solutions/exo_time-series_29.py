def get_complete_data_frame(year_begin, month_begin, year_end, month_end, columns=['t'], station=tarbes_ossun):
    res= None
    for year in range(year_begin, year_end+1):
        for month in range(month_begin if year == year_begin else 1, month_end+1 if year == year_end else 13):
            df= get_data_frame(year, month, columns, station)
            res= df if res is None else res.append(df)
    return res
