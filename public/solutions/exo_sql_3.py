import pandas as pd
import sqlite3
conn = sqlite3.connect("Data/Databases/ccs.sqlite")

tables = pd.read_sql_query("SELECT name FROM sqlite_master WHERE type='table';", conn)

# Be sure to close the connection
conn.close()
tables
