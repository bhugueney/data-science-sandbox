import matplotlib as mpl
label_size = 8
mpl.rcParams['xtick.labelsize'] = label_size
mpl.rcParams['ytick.labelsize'] = label_size
df['Global_active_power'].resample('W').sum().plot()
