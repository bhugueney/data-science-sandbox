deaths['year'] = deaths.index.year
deaths['month'] = deaths.index.month
deaths['weekday'] = deaths.index.weekday_name
deaths['dayofweek'] = deaths.index.dayofweek
deaths['quarter'] = deaths.index.quarter
deaths['dayofyear'] = deaths.index.dayofyear
deaths['dayofmonth'] = deaths.index.day
deaths['weekofyear'] = deaths.index.weekofyear
