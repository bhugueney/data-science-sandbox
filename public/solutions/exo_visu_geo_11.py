import numpy as np
from matplotlib.colors import ListedColormap

import geoplot as gplt


ax= plt.axes(projection = tiles.crs)
# 48.818644, 2.465676
# 48.899279, 2.225146
ax.set_extent([2.22, 2.46, 48.81, 48.9])#, ccrs.PlateCarree())
ax.add_image(tiles, 12)

cmap = plt.cm.cividis
my_cmap = cmap(np.arange(cmap.N))

my_cmap[:,-1] = np.linspace(0.5, 1, cmap.N)

my_cmap = ListedColormap(my_cmap)

gplt.kdeplot(geo_lights,shade=True, cbar=True,cmap=my_cmap, ax=ax, cbar_kws={'shrink' : 0.5})
