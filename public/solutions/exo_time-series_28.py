tarbes_ossun=7621
def get_data_frame(year, month, columns=['t'], station=tarbes_ossun):
    df= pd.read_csv("https://donneespubliques.meteofrance.fr/donnees_libres/Txt/Synop/Archive/synop.%d%02d.csv.gz" % (year,month), header=0, sep=";", parse_dates=['date'], date_parser=my_date_parser)
    return df.loc[df['numer_sta']==station, ['date']+columns]
