from datetime import date
def day_number_to_month_day(days, _=None):
    return date.fromordinal(date(2001, 1, 1).toordinal() + int(days) ).strftime('%m-%d')

sns.set(font_scale=3)
from matplotlib.ticker import FuncFormatter
g=sns.relplot(x='dayofyear', y='count', kind='line',data=deces, height=16, aspect=2)
formatter = FuncFormatter(day_number_to_month_day)
for ax in g.axes.flatten():
    ax.xaxis.set_major_formatter(formatter)
