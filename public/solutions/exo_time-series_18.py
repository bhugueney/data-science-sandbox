from statsmodels.graphics.tsaplots import plot_acf, plot_pacf
plot_acf(deaths['count'], lags=365*2);
plot_pacf(deaths['count'], lags=365*2);
