import numpy as np
from statsmodels.tsa.holtwinters import ExponentialSmoothing
sns.set(font_scale=1)
fig, ax = plt.subplots(figsize=(18, 6))
ax.plot(deaths_train['2016':].index, deaths_train['2016':].values);
ax.plot(deaths_test.index, deaths_test.values, label='vérité terrain');

for trend in ["add", "multiplicative"]:
    for seasonal in ["add", "multiplicative"]:
        model = ExponentialSmoothing(deaths_train, trend= trend, seasonal= seasonal, seasonal_periods=365)
        fit = model.fit()
        pred = fit.forecast(deaths_test.size)
        sse = np.sqrt(np.mean(np.square(deaths_test.values - pred.values)))
        ax.plot(deaths_test.index, pred, linestyle='--', label="trend :{}, seasonality : {} (RMSE={:0.2f}, AIC={:0.2f})".format(trend, seasonal, sse, fit.aic));
ax.legend();
ax.set_title("Holt-Winters");
