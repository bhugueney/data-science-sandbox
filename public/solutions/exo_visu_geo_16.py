import matplotlib.ticker as ticker

tmp = arrondissements.join(per_arr, on="l_ar")
# Définition de l'intervalles de valeurs pour la légende choroplèthe
vmin = tmp['Lumen'].min()
vmax = tmp['Lumen'].max()

fig, ax = plt.subplots(1)

# affichage de la carte en utilisant la variable à représenter comme `column` 
tmp.plot(column='Lumen', cmap=plt.cm.cividis, linewidth=0.8, ax=ax, edgecolor='0.8')

# ajout des détails de la figure

# suppression des axes
ax.axis('off')

# Titre
ax.set_title("Lumen", fontdict={'fontsize': '25', 'fontweight' : '3'})

# annotation sur la source des données
ax.annotate('Source: Paris Open Data, 2019',
           xy=(0.1, .2), xycoords='figure fraction',
           horizontalalignment='left', verticalalignment='top',
           fontsize=30, color='#555555')

# Légende
sm = plt.cm.ScalarMappable(cmap=plt.cm.cividis, norm=plt.Normalize(vmin=vmin, vmax=vmax))
divider = make_axes_locatable(ax)
cax = divider.append_axes("right", size="5%", pad=0.0)

def fmt(x, pos):
    a, b = '{:.2e}'.format(x).split('e')
    b = int(b)
    return r'${} \times 10^{{{}}}$'.format(a, b)

cb=plt.colorbar(sm, cax=cax,format=ticker.FuncFormatter(fmt))
cb.ax.tick_params(labelsize=30);
