sns.set(font_scale=1)
deaths_train['count'].to_frame() \
    .rename(columns={'count': 'test set'}) \
    .join(deaths_test['count'].to_frame().rename(columns={'count': 'training set'}), how='outer', lsuffix='_left', rsuffix='_right') \
    .plot(figsize=(15,5), title='deaths per day', style='.')
