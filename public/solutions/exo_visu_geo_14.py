per_arr=sjoin(geo_lights, arrondissements, how="left",op="within").groupby('l_ar').agg(
    {'Flux de la lampe (en Lumen)':'sum',
     'Identifiant unique de la lampe':'count'}).rename(columns={'Flux de la lampe (en Lumen)':'Lumen',
                                                                'Identifiant unique de la lampe':'Nb'})
per_arr
