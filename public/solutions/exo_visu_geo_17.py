tmp = arrondissements.join(per_arr, on="l_ar").to_crs('epsg:2154')
tmp['Lumen/area']= tmp['Lumen']/tmp.area

# Définition de l'intervalles de valeurs pour la légende choroplèthe
vmin = tmp['Lumen/area'].min()
vmax = tmp['Lumen/area'].max()

fig, ax = plt.subplots(1)

# affichage de la carte en utilisant la variable à représenter comme `column` 
tmp.plot(column='Lumen/area', cmap=plt.cm.cividis, linewidth=0.8, ax=ax, edgecolor='0.8')

# ajout des détails de la figure

# suppression des axes
ax.axis('off')

# Titre
ax.set_title("Lumen/area", fontdict={'fontsize': '25', 'fontweight' : '3'})

# annotation sur la source des données
ax.annotate('Source: Paris Open Data, 2019',
           xy=(0.1, .2), xycoords='figure fraction',
           horizontalalignment='left', verticalalignment='top',
           fontsize=30, color='#555555')

# Légende
sm = plt.cm.ScalarMappable(cmap=plt.cm.cividis, norm=plt.Normalize(vmin=vmin, vmax=vmax))
divider = make_axes_locatable(ax)
cax = divider.append_axes("right", size="5%", pad=0.0)

cb=plt.colorbar(sm, cax=cax)
cb.ax.tick_params(labelsize=30);
