#+TITLE: Data Acquisition (en-US)
#+AUTHOR: Bernard Hugueney
#+DATE: <2020-12-07 Mon 13:30>
#+LANGUAGE:  fr



#+BEGIN_SRC emacs-lisp :exports none :results silent
(setq ob-ipython-show-mime-types nil)
#+END_SRC


* Concepts

For the sake of simplicity, we will mostly use /csv/ files. However,
Python and Pandas enable us to get data for all kinds of sources and
in all kinds of formats. We will here focus on the following :

- [[https://en.wikipedia.org/wiki/JSON][JSON]]
- [[https://en.wikipedia.org/wiki/Web_scraping][Web Scraping]]
- [[https://en.wikipedia.org/wiki/Relational_database][Relational Databases]] with [[https://en.wikipedia.org/wiki/SQL][SQL]]

* JSON

Originally used to easily exchange data with Javascript web
applications it became widespread with [[https://en.wikipedia.org/wiki/Representational_state_transfer][REST]] web services :

- Text based
- Unstructured :
  - /Documents/ (≠ tables) 
  - Without integrity constraints (/schema/) ([[https://json-schema.org/][for now ?]])
- Very Basic types

** Text based

- encoding :: defaults to UTF-8
- human readable/editable :: possible but not recommended ([[https://www.kaggle.com/usdod/world-factbook-country-profiles/discussion/71462][mistakes]]
     always possible)
- memory intensive :: alleviated by compression

** Unstructured

Contrary to /DataFrames/ → normalizing and handling missing values

** Very Basic Data Types


- character string
- number (→ integer or floating point number)
- object (dictionary)
- array (→ list)
- Boolean
- null (→ None)

No /Date/ data type, nor missing values.


* Web Scraping

- Single page or multiple pages (respect [[https://fr.wikipedia.org/wiki/Protocole_d%27exclusion_des_robots][robots.txt]]).

- Different levels of difficulty :
  - valid XHTML
  - invalid HTML
  - authentication required

** Single or multiple pages

If there are a lot of pages to scrap, it could be useful to make a
 [[https://towardsdatascience.com/fast-and-async-in-python-accelerate-your-requests-using-asyncio-62dafca83c33][asynchronous]] (parallel) requests

** Valid XHTML

If (!) the HTML code is [[https://validator.w3.org/][valid]] (which is does not need to be for web
browsers to display it), one can trivially and efficiently scrap it
with [[https://pandas.pydata.org/pandas-docs/stable/reference/api/pandas.read_html.html][pandas.read_html]].

#+BEGIN_SRC ipython
html_table = """
<table>
  <thead>
    <tr>
      <th>Programming Languages</th>
      <th>Author</th> 
      <th>Year</th>
    </tr>
  </thead>
  <tbody>
    <tr>
      <td>C</td>
      <td>Dennis Ritchie</td> 
      <td>1972</td>
    </tr>
    <tr>
      <td>Python</td>
      <td>Guido Van Rossum</td> 
      <td>1989</td>
    </tr>
    <tr>
      <td>Lisp</td>
      <td>John McCarthy</td> 
      <td>1958</td>
    </tr>
  </tbody>
</table>
"""
#+END_SRC

#+BEGIN_SRC ipython
from IPython.display import display_html
display_html(html_table, raw=True)
#+END_SRC

** Invalid HTML

More often than not, web site do not bother to use valid HTML as long
as they can be rendered on popular web browsers. One must then use a
library to "interpret" the document (for instance [[https://fr.wikipedia.org/wiki/Beautiful_Soup][Beautiful
Soup]]). Luckily, [[https://pandas.pydata.org/pandas-docs/stable/user_guide/io.html#io-html-gotchas][pandas now does it automacitally]].

** Authentication required / proxy required

Before using /Pandas/ to construct the /DataFrame/, one must establish
the secured/proxied connection and retrieve the documents, for
instance with the [[https://requests.readthedocs.io/en/master/][requests]] library.

*Beware !* When handling passwords, *NEVER EVER* write them into
code. You should use environment variable, for instance for you [[https://requests.readthedocs.io/en/master/user/advanced/#proxies][proxy
configuration]].

* Relational Databases

Relational Databases allow efficient data selection from very large
data bases, the preprocessing being offloaded to the RDBMS. Data are
/structured/ and requests are written in [[https://en.wikipedia.org/wiki/SQL][SQL]].


* Questions ?

* Examples: using JSON


#+BEGIN_SRC ipython
import pandas as pd
import requests
#+END_SRC

#+BEGIN_SRC text  :mkdirp yes :exports none :tangle ./Data/JSON/exchangeratesapi.json
{
  "rates": {
    "CAD": 1.5497,
    "HKD": 9.2404,
    "ISK": 159,
    "PHP": 57.375,
    "DKK": 7.4415,
    "HUF": 362.32,
    "CZK": 26.213,
    "AUD": 1.6182,
    "RON": 4.8736,
    "SEK": 10.1723,
    "IDR": 16868.74,
    "INR": 88.3015,
    "BRL": 6.4021,
    "RUB": 90.9894,
    "HRK": 7.5577,
    "JPY": 124.23,
    "THB": 36.148,
    "CHF": 1.0826,
    "SGD": 1.5955,
    "PLN": 4.4907,
    "BGN": 1.9558,
    "TRY": 9.3286,
    "CNY": 7.8422,
    "NOK": 10.563,
    "NZD": 1.7,
    "ZAR": 18.2657,
    "USD": 1.1922,
    "MXN": 23.9402,
    "ILS": 3.9557,
    "GBP": 0.89442,
    "KRW": 1318.04,
    "MYR": 4.8517
  },
  "base": "EUR",
  "date": "2020-11-27"
}
#+END_SRC

#+BEGIN_SRC ipython
response = requests.get("https://api.exchangeratesapi.io/latest")
print(response.text)
# or, if network connection does not work out of the box, 
#with open("./Data/JSON/exchangeratesapi.json", 'r') as file:
#    print(file.read())
#+END_SRC


#+BEGIN_SRC ipython
exchange_rates = pd.read_json("https://api.exchangeratesapi.io/latest")
# or
#exchange_rates = pd.read_json("./Data/JSON/exchangeratesapi.json")
exchange_rates
#+END_SRC



** Flat data

https://api.github.com/repos/jupyter/notebook/issues

https://gitlab.com/api/v4/projects/4217422/issues


https://stackoverflow.com/questions/54039562/format-github-json-data-into-a-pandas-dataframe-with-daily-dates

https://www.kaggle.com/jboysen/quick-tutorial-flatten-nested-json-in-pandas

** Nested Data


* Examples : Web scraping

#+BEGIN_SRC ipython
data_frames = pd.read_html("https://fr.wikipedia.org/wiki/Python_(langage)")
# or
#data_frames = pd.read_html("./Data/HTML/Python_(langage)")
type(data_frames), type(data_frames[0])
#+END_SRC

#+BEGIN_SRC ipython
data_frames[4][['Version','Date de sortie']]
#+END_SRC

#+BEGIN_SRC ipython
data_frames = pd.read_html("https://www.nationmaster.com/nmx/ranking/expenditures-on-food")
data_frames[0].columns
#+END_SRC

The fact that /Pandas/ can automatically switch to /libhtml5/ or
/BeautifulSoup/ when encountering errors means that one might need
those libraries even if they are not hard dependencies or /Pandas/.

One can install these "just-in-time" from the Notebook, but the kernel
might need to be restarted for them to be taken into account :

#+BEGIN_SRC ipython
!pip install html5lib
#restart kernel if need be
#+END_SRC

#+BEGIN_SRC ipython
!pip install bs4
#restart kernel if need be
#+END_SRC

** Authenticated requests

If requesting data requires to be authenticated, using /Pandas/
directly causes an error : «HTTPError: HTTP Error 401: UNAUTHORIZED» :

#+BEGIN_SRC ipython
pd.read_json("https://httpbin.org/basic-auth/user/passwd", orient='index')
#+END_SRC


Using a library like [[https://requests.readthedocs.io/en/master/][requests]] allows us to make a secure authenticated
request :
#+BEGIN_SRC ipython
import requests
r = requests.get('https://httpbin.org/basic-auth/myuser/mypasswd', auth=('myuser', 'mypasswd'))
pd.read_json(r.text, orient='index')
#+END_SRC

As we have seen above, one must *NEVER EVER* write actual passwords in
code. We can use environment variables  :

#+BEGIN_SRC ipython
import os
for env_var_name, env_var_value in os.environ.items():
    print(f"var named {env_var_name} has value:{env_var_value}")
#+END_SRC

* Examples : retrieving data from a Relational Database

#+BEGIN_SRC ipython
import sqlite3

conn = sqlite3.connect(":memory:")
#+END_SRC

#+BEGIN_SRC ipython
conn.execute("CREATE TABLE prog_languages (id INTEGER PRIMARY KEY, name VARCHAR, creator VARCHAR, year YEAR);")
conn.executemany("INSERT INTO prog_languages(name, creator, year) VALUES (?,?, ?)",
                [('C', 'Dennis Ritchie', 1972),
                 ('Python', 'Guido Van Rossum', 1989),
                ('Lisp', 'John McCarthy', 1958)]);
#+END_SRC

#+BEGIN_SRC ipython
df = pd.read_sql_query("SELECT * FROM prog_languages WHERE year < 1975;", conn)
conn.close()
df
#+END_SRC

*Note :* One should close the connection to the database so as not to
 waste resources (both on the Jupyter Notebook server and on the
 database server).
