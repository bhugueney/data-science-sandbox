#+TITLE:  Time-Series (en-US)
#+AUTHOR: Bernard Hugueney
#+DATE: <2020-12-10 Thu 09:15>
#+LANGUAGE:  fr



#+BEGIN_SRC emacs-lisp :exports none :results silent
(setq ob-ipython-show-mime-types nil)
#+END_SRC


* Time-series analysis
   :PROPERTIES:
   :metadata: (slideshow . ((slide_type . slide)))
   :END:

1. Loading
2. Pre-processing
3. Descriptive Modeling
4. Predictive Modeling

* Definition
   :PROPERTIES:
   :metadata: (slideshow . ((slide_type . slide)))
   :END:

Calendar index

(Can be extracted from any dataset with dates)

* Regular vs irregular Time-Series
   :PROPERTIES:
   :metadata: (slideshow . ((slide_type . slide)))
   :END:

regular = equal time intervals

* Autocorrelation
   :PROPERTIES:
   :metadata: (slideshow . ((slide_type . slide)))
   :END:

Linear relationships between the (immediate) past and the present.
(~ linear model of regression on a /sliding window/)


* Extrapolation vs Interpolation
   :PROPERTIES:
   :metadata: (slideshow . ((slide_type . slide)))
   :END:

- It is much harder to predict the future than the past !
  - train / validation / test split must be chronological
- Especially far away :
  - (N-steps ahead vs 1-step ahead)

* Missing values
   :PROPERTIES:
   :metadata: (slideshow . ((slide_type . slide)))
   :END:

Considering the auto-correlation and the need for regular time-series
with most algorithms, it often makes sense to interpolate or /forward
fill/ the missing values instead of removing rows with missing values.

* Dates are not simple
   :PROPERTIES:
   :metadata: (slideshow . ((slide_type . slide)))
   :END:

- time zones
- leap years
- daylight saving time
- leap seconds

* Chronological Attributes
   :PROPERTIES:
   :metadata: (slideshow . ((slide_type . slide)))
   :END:

Enriching the data :
- generic (e.g. Day of week)
- location specific (e.g. holidays)


* Forecastability ?
   :PROPERTIES:
   :metadata: (slideshow . ((slide_type . slide)))
   :END:

One can try to forecast [[https://www.eia.gov/dnav/pet/hist_xls/RBRTEd.xls][all kinds of time-series]] the market prices are
not an easy target at efficient market time scales.

* Examples
   :PROPERTIES:
   :metadata: (slideshow . ((slide_type . slide)))
   :END:

** Time-Series extraction

The dataset of [[http://data.cquest.org/insee_deces/insee_deces.csv.gz][French death records]] contains the day of death so we
can generate a time-series of the number of deaths per day with a
~groupby~ :

#+BEGIN_SRC ipython
import pandas as pd
#+END_SRC

#+BEGIN_SRC ipython
# deaths= pd.read_csv("http://data.cquest.org/insee_deces/insee_deces.csv.gz")
deaths= pd.read_csv("./Data/Deces/insee_deces.csv.gz")
#+END_SRC

#+BEGIN_SRC ipython
deaths.columns
#+END_SRC

Without explicit indications, /Pandas/ will not recognize the colomns
~date_naissance~ and ~date_deces~ as dates but only as character
strings :

#+BEGIN_SRC ipython
type(deaths['date_deces'].values[0])
#+END_SRC

We can indicate at load time which columns should to read as dates
with the ~parse_dates~ argument (i.e. ~parse_dates=['date_naissance',
'date_deces']~). However, one often also has to tell /Pandas/ how to
parse those dates.  This is done with the ~date_parser~ argument. The
function given to this argument is often simple enough that we can use
a /lambda/ :

#+BEGIN_SRC ipython
deaths= pd.read_csv("./Data/Deces/insee_deces.csv.gz",header=0,parse_dates=['date_naissance', 'date_deces'],
                      date_parser= lambda x : pd.to_datetime(x, errors='coerce', format='%Y-%m-%d'),
                     dtype = {'nom' : 'str', 'prenom':'str', 'sexe':'str', 'date_naissance': 'str',
                             'code_lieu_naissance':'str', 'lieu_naissance':'str', 'pays_naissance':'str',
                             'date_deces':'str', 'code_lieu_deces':'str','numero_acte_deces':'str'})
#+END_SRC

#+BEGIN_SRC ipython
deaths.info()
#+END_SRC


One can also convert the columns after loading the dataset :

#+BEGIN_SRC ipython
deaths['date_naissance']=pd.to_datetime(deaths['date_naissance'], errors='coerce', format='%Y-%m-%d')
#+END_SRC

To create a time-series of the number of deaths per day, one can
simple use ~groupby~ :

#+BEGIN_SRC ipython
deaths=deaths.groupby('date_deces')['numero_acte_deces'].count()
#+END_SRC


The index of the resulting ~Series~ is indeed of type ~DatetimeIndex~:

#+BEGIN_SRC ipython
deaths.index
#+END_SRC


One can /slice/ using dates or even time intervals like a year or a
month :
#+BEGIN_SRC ipython
deaths['2000']
#+END_SRC

/Pandas/ can be used to display time-series without even needed
/Seaborn/ :

#+BEGIN_SRC ipython
deaths.plot()
#+END_SRC

We can see that the dataset was not accurate before the 70's and it is
easy enough to zoom in with a slice :


#+BEGIN_SRC ipython
deaths['1972':'1980'].plot()
#+END_SRC

We can reduce the dataset so as to start in year 1972 :

#+BEGIN_SRC ipython
deaths=deaths['1972':]
#+END_SRC


#+BEGIN_SRC ipython
deaths.plot()
#+END_SRC

We may have noticed that when we displayed the index, it showed
~freq=None~. In practice, we would rather work on a /regular/
time-series (same time step between any two consecutive observations,
except for missing values). This can be achieved with the ~asfreq~
method :


#+BEGIN_SRC ipython
deaths=deaths.asfreq('D')
deaths.index
#+END_SRC


With the chosen sampling frequency does not match the data, one should
rather use the [[https://pandas.pydata.org/pandas-docs/stable/user_guide/timeseries.html#resampling][resample]] method to select how the sub-sampling or
over-sampling should be done :

#+BEGIN_SRC ipython
monthly_deaths= deaths.resample('M').sum()
#+END_SRC


#+BEGIN_SRC ipython
monthly_deaths.plot()
#+END_SRC


* Multivariate Time-Series

We will now focus on the dataset from the [[https://data.open-power-system-data.org/time_series/latest/][OpenPower System Data]].

*Exercise :* Load the dataset in a ~DataFrame~ named ~ops~ making sure
that the dates columns are of the right type.

#+BEGIN_SRC ipython
ops=pd.read_csv('./Data/Open-Power-System-Data/time_series_60min_singleindex.csv.gz',parse_dates=["utc_timestamp","cet_cest_timestamp"],index_col=0)
#+END_SRC

#+BEGIN_SRC ipython
ops.info()
#+END_SRC

Displaying (part of) the ~DataFrame~ :
#+BEGIN_SRC ipython
ops
#+END_SRC

There are *a lot* of columns !

#+BEGIN_SRC ipython
pd.options.display.max_seq_items = 2000
ops.columns
#+END_SRC

We will elect a few columns about France and rename them :

#+BEGIN_SRC ipython
ops_FR=pd.DataFrame(ops[['cet_cest_timestamp','FR_load_actual_tso', 'FR_load_forecast_tso',
       'FR_solar_generation_actual', 'FR_wind_onshore_generation_actual']])
#+END_SRC


#+BEGIN_SRC ipython
ops_FR=ops_FR.rename(columns={'cet_cest_timestamp': 'date','FR_load_actual_tso': 'load_actual', 'FR_load_forecast_tso': 'load_forecast', 'FR_solar_generation_actual':'solar', 'FR_wind_onshore_generation_actual':'wind'})

#+END_SRC

#+BEGIN_SRC ipython
ops_FR.shape
#+END_SRC

*Exercise :* Display the number of missing values for each column.

#+BEGIN_SRC ipython
ops_FR.isnull().sum()
#+END_SRC

*Exercise :* Remove all lines with at least one missing value.

#+BEGIN_SRC ipython
ops_FR=ops_FR.dropna(how='any')
#+END_SRC

#+BEGIN_SRC ipython
ops_FR.shape
#+END_SRC


In fact we can see that data was only available since 2012 :

#+BEGIN_SRC ipython
ops_FR
#+END_SRC

We can see that the dates in the index and those in the ~date~ are not in the same time zone. We can convert the index (from UTC) to Paris local time :

#+BEGIN_SRC ipython
ops_FR.index = ops_FR.index.tz_convert('Europe/Paris')
ops_FR
#+END_SRC

In order to be able to select data according to chronological
attributes, one often adds such attributes that can be computed from
the date :

#+BEGIN_SRC ipython
ops_FR['year'] = ops_FR.index.year
ops_FR['month'] = ops_FR.index.month
ops_FR['weekday'] = ops_FR.index.day_name()
#+END_SRC

#+BEGIN_SRC ipython
ops_FR['dayofweek'] = ops_FR.index.dayofweek
ops_FR['hour'] = ops_FR.index.hour
ops_FR['quarter'] = ops_FR.index.quarter
ops_FR['dayofyear'] = ops_FR.index.dayofyear
ops_FR['dayofmonth'] = ops_FR.index.day
ops_FR['weekofyear'] = ops_FR.index.weekofyear
#+END_SRC

#+BEGIN_SRC ipython
ops_FR.sample(10, random_state=42)
#+END_SRC

#+BEGIN_SRC ipython
ops_FR=ops_FR.fillna(method='ffill')
ops_FR=ops_FR.dropna(how='any')
ops_FR.isnull().sum()
#+END_SRC

*Exercise :* Check if ~ops_FR~ has a frequency and if not, set an
hourly one, making sure that missing values where not introduced in
the process.


#+BEGIN_SRC ipython
ops_FR=ops_FR.asfreq('H', method='ffill')
#+END_SRC

#+BEGIN_SRC ipython
ops_FR.isnull().sum()
#+END_SRC

For visualizations, we can use /Seaborn/ like with any ~DataFrame~ :

#+BEGIN_SRC ipython
%matplotlib inline
import seaborn as sns
sns.lmplot(x="solar", y="wind", data=ops_FR);
#+END_SRC


#+BEGIN_SRC ipython
sns.pairplot(data=ops_FR, hue='year', kind="reg", vars=['load_actual','load_forecast', 'wind','solar']);
#+END_SRC

#+BEGIN_SRC ipython
sns.jointplot(y="solar", x="load_actual", data=ops_FR, kind="kde", color="#4CB391");
#+END_SRC


#+BEGIN_SRC ipython
sns.jointplot(y="solar",ylim=(0,3000), x="load_actual",xlim=(0,90000), data=ops_FR, kind="kde", color="#4CB391");
#+END_SRC

#+BEGIN_SRC ipython
sns.jointplot(y="wind",ylim=(0,6000), x="load_actual",xlim=(0,90000), data=ops_FR, kind="kde", color="#4CB391");
#+END_SRC


We can use /Pandas/ to display the time-series, selecting the value(s)
to display and the time interval. It is possible to use /matplotlib/
to customize the display from /Pandas/ :

#+BEGIN_SRC ipython
sns.set(rc={'figure.figsize':(12, 12)})
ax = ops_FR.loc['2018-01':'2018-02', ['load_forecast','load_actual']].plot()
ax.set_ylabel('Consommation électrique (GWh)');
#+END_SRC

To correctly display dates on the x-axis, we must call
~pd.plotting.register_matplotlib_converters()~:

#+BEGIN_SRC ipython
pd.plotting.register_matplotlib_converters()
ax = ops_FR.loc['2018-01':'2018-02', 'load_actual'].resample('D').mean().plot()
ax.set_ylabel('Electricity consumption (GWh)');
#+END_SRC

The synthetic chronological attributes can be useful for visualizations :


#+BEGIN_SRC ipython
sns.catplot(x='month', y='load_actual', data=ops_FR, kind='violin', height=12, col='year', col_wrap=3);
#+END_SRC

#+BEGIN_SRC ipython
import matplotlib.pyplot as plt
#+END_SRC

#+BEGIN_SRC ipython
sns.pairplot(ops_FR,
             hue='hour',
             x_vars=['hour','weekday','year','weekofyear'],
             y_vars='load_actual',
             height=5,
             plot_kws={'alpha':0.1, 'linewidth':0}
            )
plt.suptitle("Electricity consumption GW per Hour, day of week, year and week of year");
#+END_SRC

#+BEGIN_SRC ipython
ops_FR.columns
#+END_SRC


To compare different values of the multivariate time-series with
/Seaborn/, we have to reshape the "wide" format, of one column per
value, to a "long" format, with a row per value and a column for the
type of value (~'kind'~ bellow), thanks to the ~melt~ method :

#+BEGIN_SRC ipython
ops_FR_melt= pd.melt(ops_FR, id_vars=['date','year','month','weekday'],var_name='kind', value_name="GWh")
#+END_SRC

#+BEGIN_SRC ipython
ops_FR_melt=ops_FR_melt.set_index('date')
ops_FR_melt
#+END_SRC

#+BEGIN_SRC ipython
sns.catplot(x='month', y='GWh', data=ops_FR_melt, kind='violin', height=12, col='kind', col_wrap=2);
#+END_SRC

#+BEGIN_SRC ipython
sns.catplot(x='weekday', y='GWh', data=ops_FR_melt, kind='violin', height=12, col='kind', col_wrap=2);
#+END_SRC

** Sub-sampling

*Exercise :* Resample in a ~ops_FR_daily_mean~ ~DataFrame~ a
 sub-sampling of the columns ~'load_actual'~, ~'load_forecast'~,
 ~'wind'~, ~'solar'~ by taking the daily average value :

 #+BEGIN_SRC ipython
data_columns = ['load_actual', 'load_forecast','wind', 'solar']
ops_FR_daily_mean= ops_FR[data_columns].resample('D').mean()
 #+END_SRC
 
We can see the sub-sampling :

#+BEGIN_SRC ipython
import matplotlib.pyplot as plt
start, end = '2018-06-01', '2018-06-15'
fig, ax = plt.subplots()
ax.plot(ops_FR.loc[start:end, 'solar'], marker='.', linestyle='-', linewidth=0.5, label='Hourly')
ax.plot(ops_FR_daily_mean.loc[start:end, 'solar'], marker='o', markersize=8, linestyle='-', label='Daily sub-sampling')
ax.set_ylabel('Solar electricity output(GWh)')
ax.legend();
#+END_SRC

*Exercise :* Do the same for a weekly sub-sampling and display the
 three graphs :
- hourly
- daily
- weekly

#+BEGIN_SRC ipython
start, end = '2018-06', '2018-08'
#TODO
#+END_SRC




With monthly data, we can visualize the solar electricity output, wind
power and actual load :

#+BEGIN_SRC ipython
ops_FR_monthly_mean=ops_FR[data_columns].resample('M').mean()
to_draw=['solar', 'wind', 'load_actual']
for i in to_draw:
    ax=sns.lineplot(x=ops_FR_monthly_mean.index, y= i, data=ops_FR_monthly_mean)
plt.legend(labels=to_draw)
ax.set_ylabel('GW.$h^{-1}$');
#+END_SRC


#+BEGIN_SRC ipython
ops_FR['2019-02':'2019-12']
#+END_SRC

We can compute yearly averages by resampling, ~'AS'~ being an Annual
sampling for each Start of the year:


#+BEGIN_SRC ipython
ops_FR_yearly = ops_FR[data_columns].resample('AS').mean().iloc[:-1]
ops_FR_yearly
#+END_SRC

#+BEGIN_SRC ipython
ops_FR_yearly['wind+solar/load_actual%'] = 100*(ops_FR_yearly['wind'] + ops_FR_yearly['solar'])/ ops_FR_yearly['load_actual']
ops_FR_yearly = ops_FR_yearly.set_index(ops_FR_yearly.index.year)
ops_FR_yearly.index.name = 'year'
ops_FR_yearly
#+END_SRC


#+BEGIN_SRC ipython
sns.barplot(y=ops_FR_yearly['wind+solar/load_actual%'], x=ops_FR_yearly.index )
#+END_SRC

We can compute a long-term trend with a one-year sliding window :

#+BEGIN_SRC ipython
ops_FR_yearly_trend = ops_FR[data_columns].rolling(window=365*24, center=True).mean()
#+END_SRC

#+BEGIN_SRC ipython
ops_FR['solar'].plot()
ops_FR_yearly_trend['solar'].plot()
plt.legend(['hourly', 'yearly trend']);
#+END_SRC

#+BEGIN_SRC ipython
ops_FR['load_actual'].plot()
ops_FR_yearly_trend['load_actual'].plot()
plt.legend(['hourly', 'yearly trend']);
#+END_SRC


#+BEGIN_SRC ipython
from statsmodels.tsa.seasonal import seasonal_decompose
decomposed = seasonal_decompose(ops_FR['load_actual'], model='additive',freq=int(24*365))
fig = decomposed.plot()
plt.show()
#+END_SRC

#+BEGIN_SRC ipython
decomposed = seasonal_decompose(ops_FR_monthly_mean['solar'], model='additive',freq=12)
decomposed.plot()
#+END_SRC

#+BEGIN_SRC ipython
from statsmodels.graphics.tsaplots import plot_acf, plot_pacf
#plot_acf(ops_FR_weekly_mean['solar'], lags=365*2)
plot_pacf(ops_FR_monthly_mean['solar'], lags=12*2);
#+END_SRC

* Normality or lack thereof, transformations

#+BEGIN_SRC ipython
plot_pacf(ops_FR_daily_mean['solar'], lags=365*2);
#+END_SRC

#+BEGIN_SRC ipython
sns.distplot(ops_FR_monthly_mean['solar']);
#+END_SRC


#+BEGIN_SRC ipython
sns.distplot(ops_FR_monthly_mean['solar'].diff().dropna());
#+END_SRC

#+BEGIN_SRC ipython
import numpy as np
sns.distplot(np.log(ops_FR_monthly_mean['solar']));
#+END_SRC

#+BEGIN_SRC ipython
sns.distplot(np.log(ops_FR_monthly_mean['solar']).diff().dropna());
#+END_SRC


#+BEGIN_SRC ipython
plot_pacf(ops_FR_monthly_mean['load_actual']);
#+END_SRC

#+BEGIN_SRC ipython
from statsmodels.tsa.stattools import adfuller, kpss

# ADF Test
result = adfuller(ops_FR_monthly_mean['load_actual'], autolag='AIC')
print(f'ADF Statistic: {result[0]}')
print(f'p-value: {result[1]}')
for key, value in result[4].items():
    print('Critial Values:')
    print(f'   {key}, {value}')

# KPSS Test
result = kpss(ops_FR_monthly_mean['load_actual'].diff().dropna(), regression='ct') # 'c' constante, 'ct' trend
print('\nKPSS Statistic: %f' % result[0])
print('p-value: %f' % result[1])
for key, value in result[3].items():
    print('Critial Values:')
    print(f'   {key}, {value}')
#+END_SRC

* Forecasting

We will split the dataset into a training set and a test set. As we
are only being interested in predicting the future, we must assess our
models with a test set that is the future of the training set !

We can either work on monthly data :
#+BEGIN_SRC ipython
train=ops_FR_monthly_mean[:'2017']['load_actual']
test=ops_FR_monthly_mean['2018':]['load_actual']
train.isnull().sum()
#+END_SRC

Or on daily data :
#+BEGIN_SRC ipython
train=ops_FR_daily_mean[:'2017']['load_actual']
test=ops_FR_daily_mean['2018':]['load_actual']
train.isnull().sum()
#+END_SRC

** Holt-Winter model

#+BEGIN_SRC ipython
from statsmodels.tsa.holtwinters import ExponentialSmoothing
fig, ax = plt.subplots(figsize=(18, 6))
ax.plot(train['2016':].index, train['2016':].values);
ax.plot(test.index, test.values, label='vérité terrain');

for trend in ["add", "multiplicative"]:
    for seasonal in ["add", "multiplicative"]:
        model = ExponentialSmoothing(train, trend= trend, seasonal= seasonal, seasonal_periods=365)
        fit = model.fit()
        pred = fit.forecast(test.size)
        sse = np.sqrt(np.mean(np.square(test.values - pred.values)))
        ax.plot(test.index, pred, linestyle='--', label="tendance :{}, saisonalité : {} (RMSE={:0.2f}, AIC={:0.2f})".format(trend, seasonal, sse, fit.aic));
ax.legend();
ax.set_title("Holt-Winter");
#+END_SRC

* Prophet

We want a model that can take into account :
- a long term trend, but that might be piecewise
- a number of periodical cycles
- specific events

This can be achieved with a [[https://en.wikipedia.org/wiki/Generalized_additive_model][Generalized Additive Model]] and the [[https://peerj.com/preprints/3190.pdf][prophet
library]], [[https://research.fb.com/prophet-forecasting-at-scale/][published by Facebook]], [[https://medium.com/future-vision/the-math-of-prophet-46864fa9c55a][implements this kind of models]].

#+BEGIN_SRC ipython
split_date = '2017-12-31'
ops_FR_train = pd.DataFrame(ops_FR.loc[ops_FR.index <= split_date]['load_actual'])
ops_FR_test = pd.DataFrame(ops_FR.loc[ops_FR.index > split_date]['load_actual'])
#+END_SRC

#+BEGIN_SRC ipython
ops_FR_test
#+END_SRC


#+BEGIN_SRC ipython
ops_FR_test \
    .rename(columns={'load_actual': 'test set'}) \
    .join(ops_FR_train.rename(columns={'load_actual': 'training set'}), how='outer') \
    .plot(figsize=(15,5), title='electricity load', style='.');
#+END_SRC

/Prophet/ embraces [[https://en.wikipedia.org/wiki/Convention_over_configuration][convention over configuration]] so we rename our
columns accordingly :

#+BEGIN_SRC ipython
ops_FR_train.reset_index().rename(columns={'utc_timestamp':'ds','load_actual':'y'}).head()
#+END_SRC


Unlike many other forecasting models, /prophet/ [[https://github.com/facebook/prophet/issues/627][does not require data
to be normalized]].


#+BEGIN_SRC ipython
from fbprophet import Prophet
ops_FR_train.index=ops_FR_train.index.tz_localize(None)
model = Prophet()
model.fit(ops_FR_train.reset_index().rename(columns={'utc_timestamp':'ds','load_actual':'y'}))
#+END_SRC

#+BEGIN_SRC ipython
ops_FR_test.index=ops_FR_test.index.tz_localize(None)
ops_FR_test_fcst = model.predict(df=ops_FR_test.reset_index().rename(columns={'utc_timestamp':'ds'}))
#+END_SRC


#+BEGIN_SRC ipython
f, ax = plt.subplots(1)
f.set_figheight(5)
f.set_figwidth(15)
fig = model.plot(ops_FR_test_fcst, ax=ax)
plt.show()
#+END_SRC


#+BEGIN_SRC ipython
fig = model.plot_components(ops_FR_test_fcst)
#+END_SRC

#+BEGIN_SRC ipython
import datetime
f, ax = plt.subplots(1)
f.set_figheight(5)
f.set_figwidth(15)
ax.scatter(ops_FR_test.index, ops_FR_test['load_actual'], color='r', s=5)
fig = model.plot(ops_FR_test_fcst, ax=ax)
ax.set_xbound([datetime.date(2017, 12, 1), ops_FR_test.index.max()])
#+END_SRC

#+BEGIN_SRC ipython
ops_FR_test.index
#+END_SRC

#+BEGIN_SRC ipython
import datetime
f, ax = plt.subplots(1)
f.set_figheight(5)
f.set_figwidth(15)
ax.scatter(ops_FR_test.index, ops_FR_test['load_actual'],s=8, color='r')
fig = model.plot(ops_FR_test_fcst, ax=ax)
ax.set_xbound([datetime.date(2018, 1, 1), datetime.date(2018, 1, 8)])
ax.set_ybound((40000,90000))
plot = plt.suptitle('January 2018 : forecast and actual')
#+END_SRC

#+BEGIN_SRC ipython
from sklearn.metrics import mean_squared_error, mean_absolute_error
mean_squared_error(y_true=ops_FR_test['load_actual'],
                   y_pred=ops_FR_test_fcst['yhat'])
#+END_SRC

#+BEGIN_SRC ipython
mean_absolute_error(y_true=ops_FR_test['load_actual'],
                   y_pred=ops_FR_test_fcst['yhat'])
#+END_SRC

* Special days (e.g. holidays)

Many times series are impacted by human activities, themselves
impacted by holidays, so we want to be able to take them into account.
In order to take holidays into account, one must first have data about
holidays !

** Pandas

/Pandas/ gives us tools to help define holidays ourselves :

#+BEGIN_SRC ipython
import pandas.tseries.holiday
help(pandas.tseries.holiday)
#+END_SRC

#+BEGIN_SRC ipython
from pandas.tseries.holiday import AbstractHolidayCalendar, Holiday, EasterMonday, Easter
from pandas.tseries.offsets import Day, CustomBusinessDay

class FrBusinessCalendar(AbstractHolidayCalendar):
    """ Custom Holiday calendar for France based on
        https://en.wikipedia.org/wiki/Public_holidays_in_France
      - 1 January: New Year's Day
      - Moveable: Easter Monday (Monday after Easter Sunday)
      - 1 May: Labour Day
      - 8 May: Victory in Europe Day
      - Moveable Ascension Day (Thursday, 39 days after Easter Sunday)
      - 14 July: Bastille Day
      - 15 August: Assumption of Mary to Heaven
      - 1 November: All Saints' Day
      - 11 November: Armistice Day
      - 25 December: Christmas Day
    """
    rules = [
        Holiday('New Years Day', month=1, day=1),
        EasterMonday,
        Holiday('Labour Day', month=5, day=1),
        Holiday('Victory in Europe Day', month=5, day=8),
        Holiday('Ascension Day', month=1, day=1, offset=[Easter(), Day(39)]),
        Holiday('Bastille Day', month=7, day=14),
        Holiday('Assumption of Mary to Heaven', month=8, day=15),
        Holiday('All Saints Day', month=11, day=1),
        Holiday('Armistice Day', month=11, day=11),
        Holiday('Christmas Day', month=12, day=25)
    ]
#+END_SRC

However, it is often much more practical it we can reuse holidays
definitions from third-party libraries.

** Third-party holidays libraries

[[https://github.com/peopledoc/workalendar][Workalendar]] defines holidays for a lot of countries. As some holidays
are based of religious events which are themselves based on
astronomical events, computing holidays might require the installation
of libraries such as [[https://github.com/skyfielders/python-skyfield][skyfield]] or [[https://github.com/brandon-rhodes/python-sgp4][sgp4]].

#+BEGIN_SRC ipython
from workalendar.europe import France
calendar = France()
#+END_SRC

#+BEGIN_SRC ipython
holidays=[]
for y in ops_FR['year'].unique():
    holidays= holidays + calendar.holidays(y)
holidays= pd.DataFrame(holidays).rename(columns={0:'ds',1:'holiday'})
holidays
#+END_SRC

#+BEGIN_SRC ipython
model_with_holidays=Prophet(holidays=holidays)
model_with_holidays.fit(ops_FR_train.reset_index().rename(columns={'utc_timestamp':'ds','load_actual':'y'}))
#+END_SRC

#+BEGIN_SRC ipython
ops_FR_test_fcst_holidays = model_with_holidays.predict(df=ops_FR_test.reset_index().rename(columns={'utc_timestamp':'ds'}))
#+END_SRC

#+BEGIN_SRC ipython
mean_squared_error(y_true=ops_FR_test['load_actual'],
                   y_pred=ops_FR_test_fcst['yhat'])
#+END_SRC


#+BEGIN_SRC ipython
mean_squared_error(y_true=ops_FR_test['load_actual'],
                   y_pred=ops_FR_test_fcst_holidays['yhat'])
#+END_SRC


#+BEGIN_SRC ipython
mean_absolute_error(y_true=ops_FR_test['load_actual'],
                   y_pred=ops_FR_test_fcst['yhat'])
#+END_SRC

#+BEGIN_SRC ipython
mean_absolute_error(y_true=ops_FR_test['load_actual'],
                   y_pred=ops_FR_test_fcst_holidays['yhat'])
#+END_SRC


#+BEGIN_SRC ipython
import warnings
f, ax = plt.subplots(1)
f.set_figheight(5)
f.set_figwidth(15)
ax.scatter(ops_FR_test.index, ops_FR_test['load_actual'],s=8, color='r')
ax.scatter(ops_FR_test.index, ops_FR_test_fcst['yhat'],s=8, color='g')

fig = model_with_holidays.plot(ops_FR_test_fcst_holidays, ax=ax)
ax.set_xbound([datetime.date(2018, 1, 1), datetime.date(2018, 1, 8)])
ax.set_ybound((40000,90000))
plot = plt.suptitle('January 2018 : forecasts with/without holidays and actual load')
with warnings.catch_warnings(record=True):
    ax.legend(['_nolegend_', 'forecasts with holidays', 'actual', 'forecasts without holidays'])
#+END_SRC


#+BEGIN_SRC ipython
f, ax = plt.subplots(1)
f.set_figheight(5)
f.set_figwidth(15)
ax.scatter(ops_FR_test.index, ops_FR_test['load_actual'],s=8, color='r')
ax.scatter(ops_FR_test.index, ops_FR_test_fcst['yhat'],s=8, color='g')

fig = model_with_holidays.plot(ops_FR_test_fcst_holidays, ax=ax)
ax.set_xbound([datetime.date(2018, 4, 30), datetime.date(2018, 5, 5)])
ax.set_ybound((30000,70000))
plot = plt.suptitle('First of May 2018 : forecasts with/without holidays and actual')
with warnings.catch_warnings(record=True):
    ax.legend(['_nolegend_', 'forecasts with holidays', 'actual', 'forecasts without holidays'])
#+END_SRC

In order to assess the forecasts just on the data during holidays, we
build a list of all the timestamps during the holidays in our
dataset :

#+BEGIN_SRC ipython
holidays_list=[]
for i in range(holidays.index.size):
    holidays_list+=pd.date_range(holidays.iloc[i]['ds'], periods=24, freq='H')
holidays_list
#+END_SRC

We can then select the rows of our ~DataFrame~ for those timestamps :
#+BEGIN_SRC ipython
holidays_test = ops_FR_test.query('index in @holidays_list')
holidays_pred = ops_FR_test_fcst.query('ds in @holidays_list')
holidays_pred_holidays_model = ops_FR_test_fcst_holidays.query('ds in @holidays_list')
#+END_SRC

#+BEGIN_SRC ipython
mean_absolute_error(y_true=holidays_test['load_actual'], y_pred=holidays_pred['yhat'])
#+END_SRC

#+BEGIN_SRC ipython
mean_absolute_error(y_true=holidays_test['load_actual'], y_pred=holidays_pred_holidays_model['yhat'])
#+END_SRC

*Exercices :*
- Compare the results with the use of ~model.add_country_holidays(country_name='France')~
- Check the impact of the [[https://facebook.github.io/prophet/docs/seasonality,_holiday_effects,_and_regressors.html#prior-scale-for-holidays-and-seasonality][holidays.prior.scale]] parameter

* Time-Series Forecasting as Regression : XGBoost

As we have seen at the beginning, it is possible to model time series
forecasting a regression over sliding windows.
We will see how XGBoost fares :

#+BEGIN_SRC ipython
import xgboost as xgb
from xgboost import plot_importance, plot_tree
#+END_SRC

#+BEGIN_SRC ipython
ops_FR.columns
#+END_SRC

Calendar attributes can be coded as numerical values :
#+BEGIN_SRC ipython
features=['hour','dayofweek','quarter','month','year',
           'dayofyear','dayofmonth','weekofyear']
#+END_SRC

As usual for conventional machine learning algorithms, we will
normalize the data, here with [[https://scikit-learn.org/stable/modules/generated/sklearn.preprocessing.StandardScaler.html][StandardScaler]], so that the day of year
does not have a disproportionate impact compared to the day of the
week, for instance :

#+BEGIN_SRC ipython
from sklearn.preprocessing import StandardScaler
std = StandardScaler()
scaled = std.fit_transform(ops_FR[features])
scaled = pd.DataFrame(scaled,columns=features)
scaled.index=ops_FR.index.copy()
#+END_SRC

#+BEGIN_SRC ipython
ops_FR_ml=scaled.merge(ops_FR['load_actual'],left_index=True,right_index=True)
ops_FR_ml
#+END_SRC


We take holidays into account by having an ~'holidays'~ attribute to 1
during holidays and 0 otherwise :

#+BEGIN_SRC ipython
ops_FR_ml.loc[ops_FR_ml.index.isin(holidays_list),'holidays']=1.
ops_FR_ml['holidays']= ops_FR_ml['holidays'].fillna(0.)
#ops_FR_ml.loc[['2012-01-01 09:00:00+01:00','2012-01-01 10:00:00+01:00']]
#+END_SRC

We split our data into explanatory variables ~X~ and target variable
~y~ :
#+BEGIN_SRC ipython
X = ops_FR_ml.drop('load_actual',1)
y = ops_FR_ml['load_actual']
X
#+END_SRC

We split our data into a training set and a testing set :
#+BEGIN_SRC ipython
split_date = '2017-12-31'
ops_FR_ml_train=ops_FR_ml.loc[ops_FR_ml.index <= split_date]
ops_FR_ml_test=ops_FR_ml.loc[ops_FR_ml.index > split_date]
#+END_SRC


#+BEGIN_SRC ipython
ops_FR_ml_train
#+END_SRC

#+BEGIN_SRC ipython
xgb_reg = xgb.XGBRegressor(objective ='reg:squarederror', colsample_bytree = 0.3, learning_rate = 0.1, max_depth = 5, alpha = 10, n_estimators = 100)

xgb_reg.fit(ops_FR_ml_train.drop('load_actual',1), ops_FR_ml_train['load_actual'])
#+END_SRC

#+BEGIN_SRC ipython
test_pred= xgb_reg.predict(ops_FR_ml_test.drop('load_actual',1))
mean_squared_error(y_true=ops_FR_test['load_actual'], y_pred=test_pred)
#+END_SRC

We can pick some hyperparameters "at random":
#+BEGIN_SRC ipython
xgb_reg = xgb.XGBRegressor(objective ='reg:squarederror', colsample_bytree = 0.3, learning_rate = 0.1,max_depth = 5, alpha = 10, n_estimators = 100)
xgb_reg
#+END_SRC

We will optimize hyperparameters as usual with cross-validation, but
notice how the way to split is specific to time-series (with
[[https://scikit-learn.org/stable/modules/generated/sklearn.model_selection.TimeSeriesSplit.html][TimeSeriesSplit]]):

#+BEGIN_SRC ipython
from sklearn.model_selection import TimeSeriesSplit
from sklearn.model_selection import cross_val_score
from sklearn.model_selection import GridSearchCV,RandomizedSearchCV
#+END_SRC


#+BEGIN_SRC ipython
n_iter = 1000 # you DO want to reduce that if running a hosted version on mybinder
n_splits = 5 # five-fold cross-validation
random_state = 42 # or anything, but fixed for reproducibility

param_grid = {
        'silent': [False],
        'alpha': [10],
        'objective': ['reg:squarederror'],
        'max_depth': [3, 4, 5, 6],
        'learning_rate': [0.001, 0.01, 0.1, 0.2, 0.3],
        'subsample': [0.8, 0.9, 1.0],
        'colsample_bytree': [0.2, 0.3, 0.4, 0.5, 0.6, 0.7, 0.8, 0.9, 1.0],
        'colsample_bylevel': [0.4, 0.5, 0.6, 0.7, 0.8, 0.9, 1.0],
        'min_child_weight': [0.5, 1.0, 3.0, 5.0, 7.0, 10.0],
        'gamma': [0, 0.25, 0.5, 1.0],
        'reg_lambda': [0.1, 1.0, 5.0, 10.0, 50.0, 100.0],
        'n_estimators': [100]}

xgb_search = RandomizedSearchCV(xgb.XGBRegressor(objective ='reg:squarederror',
                                                 colsample_bytree = 0.3, learning_rate = 0.1,
                                                 max_depth = 5, alpha = 10, n_estimators = 100),
                                param_grid, n_iter=n_iter,
                            n_jobs=-1, verbose=2, cv=TimeSeriesSplit(n_splits=5),
                            scoring='r2', refit=True, random_state=random_state)
xgb_search.fit(ops_FR_ml_train.drop('load_actual',1), ops_FR_ml_train['load_actual'])
#+END_SRC

#+BEGIN_SRC ipython
xgb_search
#+END_SRC

#+BEGIN_SRC ipython
xgb_search.estimator
#+END_SRC

#+BEGIN_SRC ipython
test_pred= xgb_search.predict(ops_FR_ml_test.drop('load_actual',1))
mean_squared_error(y_true=ops_FR_test['load_actual'], y_pred=test_pred)
#+END_SRC

#+BEGIN_SRC ipython
mean_squared_error(y_true=ops_FR_test['load_actual'],
                   y_pred=ops_FR_test_fcst_holidays['yhat'])
#+END_SRC

#+BEGIN_SRC ipython
mean_absolute_error(y_true=ops_FR_test['load_actual'], y_pred=test_pred)
#+END_SRC

#+BEGIN_SRC ipython
f, ax = plt.subplots(1)
f.set_figheight(5)
f.set_figwidth(15)
ax.scatter(ops_FR_test.index, ops_FR_test['load_actual'],s=8, color='r')
#ax.scatter(ops_FR_test.index, ops_FR_test_fcst['yhat'],s=8, color='g')
ax.scatter(ops_FR_test.index, test_pred,s=28, color='y')

fig = model_with_holidays.plot(ops_FR_test_fcst_holidays, ax=ax)
ax.set_xbound([datetime.date(2018, 4, 30), datetime.date(2018, 5, 5)])
ax.set_ybound((30000,70000))
plot = plt.suptitle('First of May 2018 : forecasts with holidays and actual')
with warnings.catch_warnings(record=True):
    ax.legend(['_nolegend_', 'Prophet: forecasts with holidays', 'actual', 'XGBoost : forecasts with holidays'])
#+END_SRC

#+BEGIN_SRC ipython
f, ax = plt.subplots(1)
f.set_figheight(5)
f.set_figwidth(15)
ax.scatter(ops_FR_test.index, ops_FR_test['load_actual'], color='r', s=2)
ax.scatter(ops_FR_test.index, ops_FR_test_fcst_holidays['yhat'], color='b', s=2)

ax.scatter(ops_FR_test.index, xgb_search.predict(ops_FR_ml_test.drop('load_actual',1)), color='g', s=2)

ax.set_xbound([datetime.date(2017, 12, 1), ops_FR_test.index.max()])
f.legend(['actual','Prophet forecasts' ,'XGBoost forecasts']);
#+END_SRC

#+BEGIN_SRC ipython
from sklearn.metrics import mean_squared_error
print("Prophet RMSE: %f" % mean_squared_error(ops_FR_test['load_actual'],ops_FR_test_fcst_holidays['yhat'] ))
print("XGBoost RMSE: %f" % mean_squared_error(ops_FR_test['load_actual'],xgb_search.predict(ops_FR_ml_test.drop('load_actual',1)) ))
#+END_SRC

