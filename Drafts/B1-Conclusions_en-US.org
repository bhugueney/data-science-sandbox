#+TITLE: Conclusions and Future Work (en-US)
#+DATE: <2020-12-11 Fri 15:20>
#+AUTHOR: Bernard Hugueney
#+LANGUAGE: fr

#+BEGIN_SRC emacs-lisp :exports none :results silent
(setq ob-ipython-show-mime-types nil)
#+END_SRC


* Conclusions and Future Work
   :PROPERTIES:
   :metadata: (slideshow . ((slide_type . slide)))
   :END:


* Loading and Pre-processing
   :PROPERTIES:
   :metadata: (slideshow . ((slide_type . slide)))
   :END:

- Importance of formats (dates !)
- Importance of types
- Importance of missing values
- Avoid cell-based processing for efficiency reasons (→column based
  processing)

* Data Sources
   :PROPERTIES:
   :metadata: (slideshow . ((slide_type . slide)))
   :END:

- Web :
  - API Rest : JSON
  - Scrapping
- Databases

* Visualizations
   :PROPERTIES:
   :metadata: (slideshow . ((slide_type . slide)))
   :END:

- Convenience of /Seaborn/ : automate high-level graphs construction
- Flexibility of /Matplotlib/ : customizing graphs


* Descriptive Statistics
   :PROPERTIES:
   :metadata: (slideshow . ((slide_type . slide)))
   :END:

- Distribution : normal ?
- Sample vs Population : confidence interval


* Correlation
   :PROPERTIES:
   :metadata: (slideshow . ((slide_type . slide)))
   :END:

- Correlation : Pearson's r \( \in [−1,+1] \)
- Effect's size : r^2 \( \in [0,1] \)
- Significant ? : p-value (can the observed effect be an artifact of
  sampling ?)

* Regressions
   :PROPERTIES:
   :metadata: (slideshow . ((slide_type . slide)))
   :END:

- numeric target variable → linear regression, assessment :/RMSE/,
  /MAE/

- categorical target variable → logistic regression, assessment :
  cf. classification

- categorical explanatory variables → [[https://en.wikipedia.org/wiki/One-hot]['one Hot' encoding]] : dummy
  variables

* Machine Learning
   :PROPERTIES:
   :metadata: (slideshow . ((slide_type . slide)))
   :END:

- Machine Learning : parameters (&Hyper-parameters) optimization
- Split data-set :(Train / Validation) / Test
- Cross-validation : (much) more (much) smaller validation sets
- Scoring function : Trade-offs wrt optimization metric

* Scoring Functions
   :PROPERTIES:
   :metadata: (slideshow . ((slide_type . subslide)))
   :END:

- True Positive, False Positive, True Negative, False Negative
- Confusion Matrix
- Precision / Recall
- ROC / AUC
- Overfitting : good on training data but bad on validation data
  (learning by heart ≠ generalization)


* Models
   :PROPERTIES:
   :metadata: (slideshow . ((slide_type . subslide)))
   :END:

- Logistic Regression
- Decision Tree
- K-Nearest Neighbors
- …
- Ensemble Learning :
  - Random Forest
  - Boosting (XGBoost)

* Time-Series
   :PROPERTIES:
   :metadata: (slideshow . ((slide_type . slide)))
   :END:

- Dates, sampling frequency
- Missing values replacement
- Chronological attributes

* Time-Series Forecasting
   :PROPERTIES:
   :metadata: (slideshow . ((slide_type . subslide)))
   :END:

Prophet :
- Trend
- Seasonality
- Special days (e.g. holidays)
- External variables (e.g. temperature,…)

* Future Works
   :PROPERTIES:
   :metadata: (slideshow . ((slide_type . slide)))
   :END:

** Voluminous Data
   :PROPERTIES:
   :metadata: (slideshow . ((slide_type . subslide)))
   :END:

 ~DataFrame~ too big to fit in RAM.

** Geographic Data 
   :PROPERTIES:
   :metadata: (slideshow . ((slide_type . subslide)))
   :END:

and visualizations.

** Dimensionality Reduction
   :PROPERTIES:
   :metadata: (slideshow . ((slide_type . subslide)))
   :END:

Projections

** Unsupervised Learning
   :PROPERTIES:
   :metadata: (slideshow . ((slide_type . subslide)))
   :END:

Unlabeled classification : the computer 'discovers' classes from
unlabeled data.

** (Deep) Neural Networks
   :PROPERTIES:
   :metadata: (slideshow . ((slide_type . subslide)))
   :END:

Regression / Classification of very complex data (e.g. image
recognition) : [[https://en.wikipedia.org/wiki/Deep_learning][Deep Learning]]

** Natural Language Processing
   :PROPERTIES:
   :metadata: (slideshow . ((slide_type . subslide)))
   :END:

Words → vectors in a large space. cf. Deep Learning
