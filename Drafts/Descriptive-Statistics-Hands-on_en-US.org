#+TITLE: Descriptive and Inferential Statistics Hands on (en-US)
#+AUTHOR: Bernard Hugueney
#+DATE: <2020-12-08 Tue 09:00>
#+LANGUAGE:  fr


* Mean, distribution normality

Load the dataset from the file =./Data/Heights/Galton.txt=. Each row
contains an id for the family, the heights of a father, a mother, the
gender of a child of theirs, the height of this child and the number
of children of this family.

** Data Engineering

Check for missing values, incoherent values and outliers if
any. Convert the heights, given in inches, to centimeters.

#+BEGIN_SRC ipython
# TODO
#+END_SRC

#+BEGIN_SRC ipython :exports none
import pandas as pd
#+END_SRC

** Means

Compute the mean heights for men and for women in this dataset :
- Are they the same ?
- Are they /significantly/ different ?

#+BEGIN_SRC ipython
# TODO
#+END_SRC


Compute the mean heights for fathers and for sons in this dataset :
- Are they the same ?
- Are they /significantly/ different ?

#+BEGIN_SRC ipython
# TODO
#+END_SRC

** Distributions

Check if the following distributions are normal :
- Height of children
- Height of fathers
- Heights of mothers
- Heights of male children
- Heights of female children

* Proportions

Compute the acceptance ratio for women and for men in the dataset of
the file =./Data/Simpson-Paradox/UC-Berkeley-applicants-no-dept.csv=

- Are they the same ?
- Are they /significantly/ different ?

#+BEGIN_SRC ipython :exports none
berkeley= pd.read_csv("./Data/Simpson-Paradox/UC-Berkeley-applicants-no-dept.csv")
groups= berkeley.groupby(['Gender','Admit']).count()
w_acc=groups.loc['Female','Admitted']/berkeley.groupby('Gender').count().loc['Female']['Id']
m_acc=groups.loc['Male','Admitted']/berkeley.groupby('Gender').count().loc['Male']['Id']
(w_acc,m_acc)
#+END_SRC


#+BEGIN_SRC ipython :exports none
pd.crosstab(berkeley['Admit'], berkeley['Gender'])
#+END_SRC





