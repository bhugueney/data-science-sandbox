#+TITLE: Statistical Paradoxes, Regression to the Mean Hands-on (en-US)
#+AUTHOR: Bernard Hugueney
#+DATE: <2020-12-08 Tue 15:30>
#+LANGUAGE:  fr

#+BEGIN_SRC emacs-lisp :exports none :results silent
(setq ob-ipython-show-mime-types nil)
#+END_SRC


* Simpson's Paradox

We will try to figure out if UC Berkeley was fair when considering
female applicants in 1973.

Compute the acceptance ratio for men and for women in the dataset of
the file =./Data/Simpson-Paradox/UC-Berkeley-applicants-no-dept.csv=

#+BEGIN_SRC ipython
# TODO
#+END_SRC


Are they /significantly/ different ?

#+BEGIN_SRC ipython
# TODO
#+END_SRC

In order to have a more precise understanding of the situation, we
will look at the acceptance rate for each department by using the
dataset from the file
=./Data/Simpson-Paradox/UC-Berkeley-applicants.csv=.

#+BEGIN_SRC ipython
# TODO
#+END_SRC

Is this what you were expecting ? What is going on ?

** Bonus

Working with the dataset from the file
=./Data/Simpson-Paradox/Florida-death-penalty.csv=, try to figure out
if the death penalty was given to defendants with the same odds in
1976-1977 (the period covered by the dataset). If not, can you come up
with an explanation ?

#+BEGIN_SRC ipython
# TODO
#+END_SRC


Now consider the dataset from the file
=./Data/Simpson-Paradox/Florida-death-penalty-victim.csv=

#+BEGIN_SRC ipython

#+END_SRC

What is going on ? Can you come up with an explanation ?

* Berkson's Paradox

We will try to figure out how important is height to score point at basketball (from [[https://www.kaggle.com/drgilermo/nba-players-stats-20142015]]).

Using the dataset from the file
=./Data/Berkson-Paradox/players_stats.csv=, compute the correlation
between number of points scored (~PTS~ columns) and the players'
height (~Height~ columns).

#+BEGIN_SRC ipython
# TODO
#+END_SRC


What do you think of this insight ?
What is going on ?

Can you come up with other situations where this phenomenon happens ?

* Regression to the mean

We will try to figure out if the population's height is changing from
one generation to the next, focusing on the extremes :
- are the tallest families staying as tall ?
- are the shortest families staying as short ?

** Data engineering


After loading the dataset of the file =./Data/Heights/Galton.txt= :
1. check for any missing / outlier value and fix them if any
#+BEGIN_SRC ipython
# TODO
#+END_SRC
2. convert all heights from inches to centimeters
#+BEGIN_SRC ipython
# TODO
#+END_SRC
3. create a new variable ~Average~ that is the average of the height
   of the father and the height of the mother.
#+BEGIN_SRC ipython
# TODO
#+END_SRC

** Means

1. Select the families where ~Average~ is in the largest
   quartile. What are the means of heights of parents and of the
   children in this quartile ? Are they significantly different ?
#+BEGIN_SRC ipython

#+END_SRC
2. Select the families where ~Average~ is in the smallest
   quartile. What are the means of heights of parents and of the
   children in this quartile ? Are they significantly different ?
#+BEGIN_SRC ipython

#+END_SRC


What is going on ?

Can you think of other situations where this phenomenon happens ?



