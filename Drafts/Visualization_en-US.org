#+TITLE: Data Visualization with Seaborn (en-US)
#+AUTHOR: Bernard Hugueney
#+DATE: <2020-12-07 Mon 15:20>
#+LANGUAGE:  fr



#+BEGIN_SRC emacs-lisp :exports none :results silent
(setq ob-ipython-show-mime-types nil)
#+END_SRC


* Visualization

Data Visualization in Python is mainly done thanks to [[https://matplotlib.org/][Matplotlib]]. For
both convenience and aesthetic purposes, one often uses another
library on top of /Matplotlib/, for instance [[https://seaborn.pydata.org/][Seaborn]].  For very simple
and basic visualizations, one can simply use [[https://pandas.pydata.org/pandas-docs/stable/user_guide/visualization.html][Pandas' visualization
functions]] (also based on /Matplotlib/).

However, even when using /Seaborn/, it is often necessary to 'peek
under the hood' (i.e. /Matplotlib/) to customize some parameters.

[[file:img/visu-plots.png]]

* General Principles

** Customizing and composing : abstraction levels

Figures can be infinitely complex in customization and
composition. /Seaborn/ simplifies the most common uses by offering
[[https://en.wikipedia.org/wiki/Leaky_abstraction][leaky abstractions]]. Most often, we will call /Seaborn/ functions that
call /Seaborn/ functions that call /Matplotlib/ functions !

We can :
- pass arguments that will directly be used by the /Seaborn/ function
  that we are calling,
- pass arguments that will be forwarded to underlying functions called
  indirectly (by that /Seaborn/ function that we are calling)
- call lower-level /Seaborn/ functions or even /Matplotlib/ functions
  ourselves.

** Figures and Axes

A graph can require customization at various levels depending on it
being a composite graph and/or a component of a more complex graph.
We distinguish the two following levels :

1. axes : can be a component of a more complex figure
2. figure : composed by one or more graphs on axes

Graphs on the first level can take an ~ax~ named argument to overlay
more than one graph (on the same /axes/). Graphs on the second level
give access to the underlying graphs that compose them. In all cases,
one can go down to the level of /Matplotlib/ to customized very
detail. The various customization point are illustrated below :

[[https://matplotlib.org/_images/anatomy1.png]]


* Data

Often, /Seaborn/ functions can receive data in two ways :

1. directly with ~Series~, or even ~ndarray~
2. with the name of a columns and an argument named ~data~ set to the
   ~DataFrame~ containing the column(s). /Seaborn/ will them
   automatically use the name of the column(s) for legend and/or axis
   labels.


It is important to understand that documentations on [[https://pbpython.com/effective-matplotlib.html][matplotlib graphs
customization]] are also useful for graphs made with /Seaborn/ because
graphs made with /Seaborn/ are in fact made by /Matplotlib/.

[[https://pbpython.com/images/matplotlib-pbpython-example.png]]

For instance, one can [[https://drawingfromdata.com/pandas/seaborn/matplotlib/visualization/2020/11/01/setting-figure-size-matplotlib-seaborn.html][set the figures size]] at the /Seaborn/ level as
well as at the/Matplotlib/ level.

We start by having /Jupyter/ display /Matplotlib/ graphs (hence
/Seaborn/ graphs) displayed directly in the /Notebook/, and we import
the ~seaborn~ module while giving it the ~sns~ alias :

#+BEGIN_SRC ipython
%matplotlib inline
import seaborn as sns
#+END_SRC

In fact, we also import right away a part of /Matplotlib/ : ~pyplot~,
first to set graphs size :

#+BEGIN_SRC ipython
import matplotlib.pyplot as plt
plt.rcParams['figure.figsize'] = [16, 16]
#+END_SRC

* Style

/Seaborn/ also allows to define a global drawing style (color palette,…):

#+BEGIN_SRC ipython
sns.set_style('dark')
sns.set_context('notebook')
#+END_SRC

Instead of using multiple specific functions, one can use the [[https://seaborn.pydata.org/generated/seaborn.set.html#seaborn.set][set]] function to replace the previous function calls with :

#+BEGIN_SRC ipython
sns.set(context='notebook', style='whitegrid')
#+END_SRC



#+BEGIN_SRC ipython
tips = sns.load_dataset("tips")
sns.relplot(x="total_bill", y="tip", col="time",
            hue="smoker", style="smoker", size="size",
            data=tips);
#+END_SRC

*Exercise :* Change the style ('white', 'dark', or 'darkgrid') and rerun the previous drawing.

* Visualizing the data distribution

** Loading data

#+BEGIN_SRC ipython
import pandas as pd
marathon_data = pd.read_csv('./Data/Marathon/marathon_results_2017.csv')
#+END_SRC

#+BEGIN_SRC ipython
marathon_data.columns
#+END_SRC

#+BEGIN_SRC ipython
marathon_data['Official Time']
#+END_SRC

** Utility Functions and data structures

#+BEGIN_SRC ipython
def to_sec(x):
    return x.seconds
#+END_SRC

#+BEGIN_SRC ipython
marathon_data['Official Time']= pd.to_timedelta(marathon_data['Official Time']).apply(lambda x : x.seconds)
marathon_data['Official Time']
#+END_SRC

#+BEGIN_SRC ipython
def age_to_age_group(a):
    a=a//10
    return "%d→%d"% (a*10, (a+1)*10)
marathon_data['age_group']=marathon_data['Age'].apply(age_to_age_group)
marathon_data['age_group']
#+END_SRC

#+BEGIN_SRC ipython
age_group_order= [age_to_age_group(a) for a in range(10, 90, 10)]
#+END_SRC

#+BEGIN_SRC ipython
age_group_order=[]
for a in  range(10, 90, 10):
    age_group_order.append(age_to_age_group(a))
#+END_SRC

#+BEGIN_SRC ipython
age_group_order
#+END_SRC

* Frequency distribution for a continuous variable : distplot

#+BEGIN_SRC ipython
help(sns.distplot)
#+END_SRC


#+BEGIN_SRC ipython
sns.distplot(marathon_data['Official Time'])
#+END_SRC

#+BEGIN_SRC ipython
sns.distplot(marathon_data['Official Time'], kde=False)
#+END_SRC

#+BEGIN_SRC ipython
sns.distplot(marathon_data['Official Time'], kde=False, rug=True)
#+END_SRC

#+BEGIN_SRC ipython
sns.distplot(marathon_data['Official Time'], kde=False, bins=200)
#+END_SRC

#+BEGIN_SRC ipython
sns.distplot(marathon_data['Official Time'], kde=False, bins=200)
half_hour= 60*60 / 2
x=0
x_max= marathon_data['Official Time'].max()
x_min= marathon_data['Official Time'].min()

while x <= x_max:
    if x > x_min:
        plt.axvline(x)
    x+= half_hour
#+END_SRC

If we want to set the color of the vertical lines, we just need to
pass a ~color=~ argument to ~plt.axvline~. The [[https://matplotlib.org/3.1.1/tutorials/colors/colors.html][color argument value]]
can be a color name or a tuple of (Red, Green, Blue) or (Red, Green,
Blue, Alpha) values in \([0,1]\). The RGB value can be found with any
[[https://htmlcolorcodes.com/fr/selecteur-de-couleur/][online colorpicker]].

#+BEGIN_SRC ipython
def secs_to_str(x, pos):
    'The two arguments are the value and the tick position'
    h= x // 3600
    m= (x-h*3600) // 60
    s= x % 60
    return '%02d:%02d:%02d' % (h, m, s)
secs_to_str(marathon_data['Official Time'][0], None)
#+END_SRC

#+BEGIN_SRC ipython
def time_ticks(time_min, time_max, time_inc):
    res=[]
    t=0
    while t<= time_max:
        if t >= time_min:
            res.append(t)
        t+= time_inc
    return res
time_ticks(marathon_data['Official Time'].min(),marathon_data['Official Time'].max(), 60*30 )
#+END_SRC

#+BEGIN_SRC ipython
from matplotlib.ticker import FuncFormatter
ax=sns.distplot(marathon_data['Official Time'], kde=False, bins=200)
plt.xticks(time_ticks(marathon_data['Official Time'].min(),marathon_data['Official Time'].max(), 60*30))
formatter = FuncFormatter(secs_to_str)
ax.xaxis.set_major_formatter(formatter)
#+END_SRC

#+BEGIN_SRC ipython
sns.distplot(marathon_data['Official Time'])
#+END_SRC

#+BEGIN_SRC ipython
sns.kdeplot(marathon_data['Official Time'], bw=128, lw=4, label="Temps officiel")
#+END_SRC

#+BEGIN_SRC ipython
sns.distplot(marathon_data["Official Time"], kde=True, kde_kws={'bw':128, 'lw':4, 'label': "Temps officiel"})
#+END_SRC

#+BEGIN_SRC ipython
ax=sns.distplot(marathon_data['Official Time'], kde=True, kde_kws={'bw':32, 'lw':4, 'label': "bw= 32"})
sns.kdeplot(marathon_data['Official Time'], bw=128, lw=2, label="bw= 128", color='red')
#+END_SRC

#+BEGIN_SRC ipython
sns.catplot(x='M/F', kind="count", data=marathon_data, col='age_group',
            col_wrap=4,col_order=age_group_order)
#+END_SRC

While ~catplot~ allowed us to make multiple sub-figure automatically
with the ~col~ argument (we could have used the ~row~ argument for
vertical stacking or combine both for two categories, one for rows and
one for columns). We can use /Seaborn/ to make such a figure ourselves
with ~FacetGrid~ :

#+BEGIN_SRC ipython
g = sns.FacetGrid(marathon_data, col="M/F", height=12, col_order=['F', 'M'])
g.map(sns.distplot, "Official Time");
#+END_SRC

#+BEGIN_SRC ipython
fig, axs = plt.subplots(ncols=3)
sns.distplot(marathon_data.loc[marathon_data['M/F']=='F','Official Time'], kde=False, ax= axs[0])
sns.distplot(marathon_data.loc[marathon_data['M/F']=='M','Official Time'], kde=False, ax= axs[1])
sns.distplot(marathon_data['Official Time'], kde=False, ax= axs[2])
axs[0].legend(["# of women"])
axs[1].legend(["# of men"])
axs[2].legend(["Total"])
(y_min, y_max)= axs[2].get_ylim()
for ax in axs:
    ax.set_xlim(marathon_data['Official Time'].min(), marathon_data['Official Time'].max())
    ax.set_ylim(y_min, y_max)
#+END_SRC

#+BEGIN_SRC ipython
sns.distplot(marathon_data['Official Time'], kde=False, color='g')

plt.xlabel('Temps mesuré en secondes', fontdict= {'size':14})
plt.ylabel('Effectif', fontdict= {'size':14})
plt.title('Histogramme des temps du marathon de Boston',fontsize=18)
#+END_SRC

* Frequency distribution for a discrete variable : countplot


#+BEGIN_SRC ipython
ax = sns.countplot(x='Age', hue='M/F', data=marathon_data, palette={'F':'r','M':'b'}, saturation=0.6)
ax.set_title('# by age for women and for men', fontsize=25)
ax.set_xlabel('Ages',fontdict={'size':20})
ax.set_ylabel('#',fontdict={'size':20})
ax.legend(fontsize=16)
#+END_SRC

#+BEGIN_SRC ipython
age_counts=marathon_data.groupby(['M/F','Age']).size().reset_index().pivot(columns='M/F', index='Age', values=0)
age_counts.fillna(0, inplace=True)
#+END_SRC

#+BEGIN_SRC ipython
age_counts['Total']=age_counts['M']+age_counts['F']
age_counts
#+END_SRC

#+BEGIN_SRC ipython
#Plot 1 - background - "total" (top) series
sns.barplot(x = age_counts.index, y = age_counts.Total, color = "blue")

#Plot 2 - overlay - "bottom" series
bottom_plot = sns.barplot(x = age_counts.index, y=age_counts['F'], color = "red")


topbar = plt.Rectangle((0,0),1,1,fc="blue", edgecolor = 'none')
bottombar = plt.Rectangle((0,0),1,1,fc="red",  edgecolor = 'none')
l = plt.legend([bottombar, topbar], ['Women', 'Men'], loc=1, ncol = 2, prop={'size':16})
l.draw_frame(False)

#Optional code - Make plot look nicer
sns.despine(left=True)
bottom_plot.set_ylabel("#",fontdict={'size':20})
bottom_plot.set_xlabel("Age",fontdict={'size':20})

#Set fonts to consistent 16pt size
for item in (bottom_plot.get_xticklabels() + bottom_plot.get_yticklabels()):
    item.set_fontsize(8)
#+END_SRC

* Bivariate Distribution

#+BEGIN_SRC ipython
sns.scatterplot(x='Official Time', y='Age', data=marathon_data,hue='M/F', alpha=1., style='M/F')
#+END_SRC


*Exercise :* Complete the ~DataFrame~ with data from other years and
use the ~style~ argument to represent the year.


#+BEGIN_SRC ipython
sns.jointplot(x='Official Time', y='Age', data=marathon_data, marker="+",alpha=1.)
#+END_SRC


#+BEGIN_SRC ipython
sns.jointplot(x='Official Time', y='Age', data=marathon_data, kind='hex',alpha=1.)
#+END_SRC

One can easily customize the axes of the result of ~jointplot~ :
#+BEGIN_SRC ipython
g=sns.jointplot(x='Official Time', y='Age', data=marathon_data, kind='hex',alpha=1., height=18)
g.ax_joint.set_xticks(time_ticks(marathon_data['Official Time'].min(),marathon_data['Official Time'].max(), 60*30))
g.ax_joint.xaxis.set_major_formatter(formatter)
#+END_SRC

#+BEGIN_SRC ipython
sns.jointplot(x='Official Time', y='Age', data=marathon_data, kind='kde',alpha=1.,
              height=18)
#+END_SRC


#+BEGIN_SRC ipython
ax= sns.scatterplot(x='Official Time', y='Age', data=marathon_data,hue='M/F', alpha=1., marker='+')
sns.kdeplot(marathon_data['Official Time'],marathon_data['Age'], shade=True,alpha=0.85, ax=ax)
sns.rugplot(marathon_data['Official Time'], color="g", ax=ax)
sns.rugplot(marathon_data['Age'], vertical=True, ax=ax)
#+END_SRC


#+BEGIN_SRC ipython
g=sns.jointplot(x='Official Time', y='Age', data=marathon_data, kind='kde')
g.plot_joint(plt.scatter, c="w", s=3, linewidth=1, marker="+", alpha=0.2)
#g.ax_joint.collections[0].set_alpha(0)
#+END_SRC

#+BEGIN_SRC ipython
g=sns.jointplot(x='Official Time', y='Age', data=marathon_data, kind='kde', height=12)
g.plot_joint(plt.scatter, c="w", s=3, linewidth=1, marker="+", alpha=0.2)
#+END_SRC

#+BEGIN_SRC ipython
help(axs[0].legend)
#+END_SRC


#+BEGIN_SRC ipython
g = sns.catplot("M/F", "Official Time", "age_group", 
                    data=marathon_data, kind="bar",
                    height=16,order=['F','M'], hue_order=age_group_order)
#+END_SRC


#+BEGIN_SRC ipython
g = sns.catplot("age_group", "Official Time", "M/F",
                    data=marathon_data, kind="bar",
                    height=16, palette="muted", order=age_group_order)
#+END_SRC


#+BEGIN_SRC ipython
sns.catplot(x='M/F', y='Official Time',
            kind='strip', # default value
            data=marathon_data,jitter='0.35', height=16)
#+END_SRC

*Exercise :* Make a /jitter plot/ with age categories.


#+BEGIN_SRC ipython
sns.catplot(x='Official Time',y='M/F',
            kind='swarm',
            data=marathon_data, height=16)
#+END_SRC


#+BEGIN_SRC ipython
sns.catplot(x='M/F', y='Official Time', kind='box',
            data=marathon_data,height=5, aspect= 1.5)
#+END_SRC


#+BEGIN_SRC ipython
sns.catplot(x='M/F', y='Official Time', kind='box',
            data=marathon_data,height=5, aspect= 1.5)
sns.stripplot(x='M/F', y='Official Time', data=marathon_data, alpha=0.1,jitter=0.2, color='k');
#+END_SRC

#+BEGIN_SRC ipython
sns.catplot(x='M/F', y='Official Time', kind='boxen',
            data=marathon_data,height=5, aspect= 1.5)
#+END_SRC

#+BEGIN_SRC ipython
sns.catplot(x='M/F', y='Official Time', kind='violin',
            data=marathon_data,height=5, aspect= 1.5)
#+END_SRC

#+BEGIN_SRC  ipython
sns.catplot(x='age_group', y='Official Time', kind='violin',hue='M/F', split=True
            ,data=marathon_data,height=5, aspect= 1.5, order=age_group_order, inner='stick')
#+END_SRC

#+BEGIN_SRC ipython
sns.catplot(x="M/F", kind="count", data=marathon_data)
#+END_SRC


#+BEGIN_SRC ipython
sns.catplot(x="age_group", y="Official Time", hue="M/F", kind="point", markers=["^", "o"], linestyles=["-", "--"],
            data= marathon_data, order=[age_to_age_group(a) for a in range(10, 90, 10)], height=12);
#+END_SRC

#+BEGIN_SRC ipython
sns.catplot(x="age_group", y="Official Time", kind="point", markers=["^", "o"], linestyles=["-", "--"],
            data= marathon_data, order=[age_to_age_group(a) for a in range(10, 90, 10)], height=12);
#+END_SRC


#+BEGIN_SRC ipython
sns.catplot(x="age_group", y="Official Time", col="M/F", kind="point", markers=["^", "o"], linestyles=["-", "--"],
            data= marathon_data, order=[age_to_age_group(a) for a in range(10, 90, 10)], height=12);
#+END_SRC


* Visualizing many variables : pairplot

#+BEGIN_SRC ipython
diamonds = sns.load_dataset("diamonds")
#+END_SRC

#+BEGIN_SRC ipython
sns.pairplot(diamonds)
#+END_SRC

#+BEGIN_SRC ipython
f,ax = plt.subplots(figsize=(12,8))
sns.heatmap(diamonds.corr(), annot = True,  fmt = '.2f', ax=ax)
#+END_SRC
