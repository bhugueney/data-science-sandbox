#+TITLE: Data-Science : What / Why / How ? (en-US)
#+AUTHOR: Bernard HUGUENEY
#+DATE: <2020-12-07 Mon 09:15>

#+BEGIN_SRC emacs-lisp :exports none :results silent
(setq ob-ipython-show-mime-types nil)
#+END_SRC


* Data-Science : What / Why / How ?
   :PROPERTIES:
   :metadata: (slideshow . ((slide_type . slide)))
   :END:


* Domain Knowledge × Statistics × Programming
   :PROPERTIES:
   :metadata: (slideshow . ((slide_type . slide)))
   :END:

#+attr_ipynb: (hideCode . true) (hidePrompt . true)
#+BEGIN_SRC ipython :exports results :display image/png :restart
import matplotlib.pyplot as plt
from matplotlib_venn import venn3
plt.figure(figsize=(10,10));plt.rc('font', size=20)
data_science=5
venn3((10,10,10,10,10,10,data_science), set_labels = ('Domain Knowledge', 'Statistics', 'Programming')
      , subset_label_formatter= lambda x: "Data Science" if x==data_science else None);
#+END_SRC


Note :

Programming ≠ "Computer science"


* The Computer Revolution has not happened yet !
   :PROPERTIES:
   :metadata: (slideshow . ((slide_type . slide)))
   :END:

[[https://www.computer.org/csdl/proceedings-article/icse/1999/21540584/12OmNvlxJz1][Cf. Printing Press Revolution]].

* Computer has a tool for understanding
   :PROPERTIES:
   :metadata: (slideshow . ((slide_type . slide)))
   :END:

Cognitive enhancement :

- Perceiving 
- Processing
- Modeling
- Validating Models
- Predicting

Maximizing computer ⟷ brain bandwidth : Visualization

** Perceiving
   :PROPERTIES:
   :metadata: (slideshow . ((slide_type . subslide)))
   :END:

Data Input : Acquisition / Reading

** Processing
   :PROPERTIES:
   :metadata: (slideshow . ((slide_type . subslide)))
   :END:

- Handling various data formats
- Merging various data sources
- Handling missing values / outliers

** Modeling
   :PROPERTIES:
   :metadata: (slideshow . ((slide_type . subslide)))
   :END:

- Descriptive / Predictive Modeling

- Concepts → Equivalence Class

- Figuring out an underlying structure



** Validating Models
   :PROPERTIES:
   :metadata: (slideshow . ((slide_type . subslide)))
   :END:

*** Validating descriptive models

Materialistic Cognition (≠ idealistic) : /likelihood/ of a model given
observations instead of /probability/ of events given a model.


*** Validating predictive models

A predictive model must be valid for data that have not been used to build the model (learning an underlying structure instead of memorizing by heart).


** Predicting
   :PROPERTIES:
   :metadata: (slideshow . ((slide_type . subslide)))
   :END:

We try to minimize the cost of prediction errors : trade-offs between
false positives and false negatives.

** Understanding ?
   :PROPERTIES:
   :metadata: (slideshow . ((slide_type . subslide)))
   :END:

Correlation ≠ Causality. Data Analysis is better at asking relevant
questions than giving explanations.

Cf. Domain Knowledge !


* Statistics
   :PROPERTIES:
   :metadata: (slideshow . ((slide_type . slide)))
   :END:

In practice, data rarely follows mathematical distributions for which
we have mathematical proofs. /The proof of the pudding is in the
eating./

* Tools : Python Notebooks
   :PROPERTIES:
   :metadata: (slideshow . ((slide_type . slide)))
   :END:

** Python

An *interpreted* language with an extremely large ecosystem for data
analysis. For performance reasons, one cannot process large amounts of
data *in* Python, but *with* Python thanks to libraries (that are not
written in Python).


[[https://imgs.xkcd.com/comics/compiling.png]]



* Coding vs Software Development 
   :PROPERTIES:
   :metadata: (slideshow . ((slide_type . slide)))
   :END:

We avoid the two main difficulties of software development :


- algorithmic complexity : we offload the problem to libraries writers.
- architectural complexity : we do not write software that will be
  maintained over time by a team.

* StackOverflow Oriented Development
   :PROPERTIES:
   :metadata: (slideshow . ((slide_type . slide)))
   :END:

When it is mostly a matter of correctly calling library functions, one
can easily find code snippets as answers to the same questions / error
messages

[[file:img/StackOverflow-Search.png]]

* Amount of available libraries
   :PROPERTIES:
   :metadata: (slideshow . ((slide_type . slide)))
   :END:

/Just In Time/ Learning (on the job) instead of /Just In Case/.


* Empowered vs Knowledgeable
   :PROPERTIES:
   :metadata: (slideshow . ((slide_type . slide)))
   :END:

Distinguishing between what one can do and what one knows how to do :
learning by doing !


* Minimal Essential Knowledge
   :PROPERTIES:
   :metadata: (slideshow . ((slide_type . slide)))
   :END:

Levels of detail :

1. *What we want to do*
2. *Concepts involved*
3. *Python libraries implementing the concepts*
4. Libraries functions to use
5. How to use those functions

** What we want to do
   :PROPERTIES:
   :metadata: (slideshow . ((slide_type . subslide)))
   :END:

1. Data input
2. Data cleansing (missing values / outliers)
3. Data description :
   - mathematical (models)
   - visual
4. Inferring properties :
   - of the population when we only have a sample
   - of future events when we only have past events
5. Prevision / forecasting

** Data Input
   :PROPERTIES:
   :metadata: (slideshow . ((slide_type . subslide)))
   :END:

Concept of *DataFrame* (cf. spreadsheet, tables of RDBMS), with an
*index* and *colonnes*, implemented by the [[https://pandas.pydata.org/][Pandas]] library.

** Data cleansing
   :PROPERTIES:
   :metadata: (slideshow . ((slide_type . subslide)))
   :END:

Concepts of data *types* and *missing values*. The amount of data
requires efficient data structures implemented by the [[https://numpy.org/][Numpy]] library
(used by [[https://pandas.pydata.org/][Pandas]]).

** Data Description
   :PROPERTIES:
   :metadata: (slideshow . ((slide_type . subslide)))
   :END:

Numerical or categorical (symbolical) attributes.


** Mathematical modeling
   :PROPERTIES:
   :metadata: (slideshow . ((slide_type . subslide)))
   :END:

Concepts of percentiles and *distribution*, with [[https://en.wikipedia.org/wiki/Moment_(mathematics)][moments]] : /mean/,
/variance/, /skewness/, /kurtosis/.

Correlations (/effect size/, /p-value/) implemented by [[https://www.scipy.org/][SciPy]] and
[[https://www.statsmodels.org/][Statsmodels]].

** Visualization
   :PROPERTIES:
   :metadata: (slideshow . ((slide_type . subslide)))
   :END:

Graphical representations of data with numerical and/or categorical
attributes, that one can customize / annotate / configure thanks to
[[https://seaborn.pydata.org/][Seaborn]] based on [[https://matplotlib.org/][Matplotlib]].

** Inferring properties
   :PROPERTIES:
   :metadata: (slideshow . ((slide_type . subslide)))
   :END:

Cf. mathematical modeling, still with [[https://www.statsmodels.org/][Statsmodels]].

** Forecasting (machine learning)
   :PROPERTIES:
   :metadata: (slideshow . ((slide_type . subslide)))
   :END:

Concepts :
- Forecasting the values of attributes, that are either numerical
  (*regression*) or categorical (*classification*)
- Assess numerical (*Root Mean Square Error*, *Mean Absolute Error*)
  or categorical (*precision / recall* trade-off, *ROC* & *AUC*
  curves)
- using data subsets for :
  1. *learning*
  2. *validation*
  3. *test*

Implemented in Python by the [[https://scikit-learn.org/stable/][Scikit-learn]] library (Python module named
~sklearn~)

* Pedagogy
   :PROPERTIES:
   :metadata: (slideshow . ((slide_type . slide)))
   :END:

Goals :
- Becoming autonomous, hence self-learning
- Learning as efficiently (hence as fast) as possible

* Learning curve (cognitive load)
   :PROPERTIES:
   :metadata: (slideshow . ((slide_type . slide)))
   :END:
#+attr_ipynb: (hideCode . true) (hidePrompt . true)
#+BEGIN_SRC ipython
import matplotlib.pyplot as plt
import numpy as np
import scipy.stats as stats
import math

plt.xkcd()
plt.figure(figsize=(20,10));
plt.rc('font',size=32)

#plt.rcParams['lines.linewidth'] = 10
plt.rc('lines', linewidth = 10)

mu = 0
variance = 1
sigma = math.sqrt(variance)
x = np.linspace(mu - 3*sigma, mu + 3*sigma, 40)
plt.plot(x, stats.norm.pdf(x, mu, sigma))
plt.xlabel("intensity / cognitive load")
plt.ylabel("learning")
plt.tick_params(axis='x', which='both', bottom=False, top=False, labelbottom=False)
plt.tick_params(axis='y', which='both', right=False, left=False, labelleft=False)
for pos in ['right','top','bottom','left']:
    plt.gca().spines[pos].set_visible(False)
plt.axvline(-1, color='k', linestyle='solid')
plt.axvline(1, color='k', linestyle='solid')
plt.text(-2.5,0.45,'Boredom')
plt.text(1.5,0.45,'Stress')
plt.annotate('Are you here ?', xy=(0, 0.4), xytext=(-0.75, 0.2),c='red',
            arrowprops=dict(facecolor='red', arrowstyle='fancy, tail_width=0.8, head_length=2, head_width=1.8'))

plt.show()
#+END_SRC

* Pedagogy
   :PROPERTIES:
   :metadata: (slideshow . ((slide_type . subslide)))
   :END:

- For each module
  1. Tutorial :
     - interactive
     - with small exercises
  2. Hands-on : more or less (more and more !) autonomous practice
  3. Mini quiz

- End of session : Mini project

* Questions ?
   :PROPERTIES:
   :metadata: (slideshow . ((slide_type . subslide)))
   :END:

(The right answer is "Yes !" ☺)

#+BEGIN_QUOTE
If you want to build a ship, don't drum up the men to gather wood,
divide the work and give orders. Instead, teach them to yearn for the
vast and endless sea. ---Antoine de Saint-Exupéry
#+END_QUOTE
